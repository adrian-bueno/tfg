import * as mongoose from 'mongoose';

/**
 * Initizalizes a connection with MongoDB.
 * Returns db connection.
 */
export function databaseConnect(dbUrl: string): any {
    mongoose.connect(dbUrl);

    mongoose.connection.on("error", () => {
        console.log("\x1b[1;31m" + "Connection to database : ERROR" + "\x1b[0m");
    });

    mongoose.connection.once("open", () => {
        console.log("Connection to database : OK");
    });

    return mongoose.connection;
}
