import * as fs from "fs-extra";
let Jimp = require("jimp");
let ColorThiefJimp = require("color-thief-jimp");
let sharp = require('sharp');

// TODO: Cambiar nombre clase por ImageUtils o parecido
export class ImageUtils {

    /**
     * Returns image dominant color and the recommended text color for that color.
     */
    public static getDominantColor(jimpImage: any): string[] {
        let dominantColor = ColorThiefJimp.getColor(jimpImage);
        dominantColor = `rgb(${dominantColor[0]}, ${dominantColor[1]}, ${dominantColor[2]})`;

        return [dominantColor, this.getNormalizedTextColor(dominantColor)];
    }

    /**
    * Helper function to calculate yiq - http://en.wikipedia.org/wiki/YIQ
    * https://github.com/briangonzalez/jquery.adaptive-backgrounds.js
    */
    private static getYIQ(colorRGB: string): number {
        var rgb: any = colorRGB.match(/\d+/g);
        return ((rgb[0] * 299) + (rgb[1] * 587) + (rgb[2] * 114)) / 1000;
    }

    /**
    * https://github.com/briangonzalez/jquery.adaptive-backgrounds.js
    */
    public static getNormalizedTextColor(colorRGB: string): string {
        if (this.getYIQ(colorRGB) >= 128)
            return "#111"; // dark
        else
            return "#eee"; // light
    }

    // Convertir imagen a distintas calidades
    public static processImage(imageDirPath, imageName, imageType, callback) {

        let imageFullPath = `${imageDirPath}/${imageName}.${imageType}`;

        let readableStream = fs.createReadStream(imageFullPath);
        // let qualities = [50, 100, 300, 500, 1000, 2000];
        let qualities = [64, 128, 256, 512, 1024, 2048];

        let image = sharp(imageFullPath);
        image.metadata().then(metadata => {
            let max = (metadata.width > metadata.height) ? metadata.height : metadata.width;
            let qs = [];

            for (let q of qualities)
                if (q < max) qs.push(q);
            if (max < 2000)
                qs.push(max);

            console.log(qs);

            for (let q of qs) {
                // let writableStream = fs.createWriteStream(imageDirPath + '/' + imageName + '-' + q + '.' + imageType);
                let writableStream = fs.createWriteStream(`${imageDirPath}/${q}.${imageType}`);

                const transformer = sharp()
                    .jpeg({ quality: 98 })
                    .resize(q, q)
                    .crop(sharp.gravity.center)
                    .on('error', (err) => {
                        console.log(err);
                    });

                readableStream.pipe(transformer).pipe(writableStream);
            }

            callback(undefined, qs);

        }).catch((error) => {
            // console.log(error);
            callback(error, undefined);
        });
    }
}
