import * as passport                       from "passport";
import { Strategy as LocalStrategy }       from "passport-local";
import { BasicStrategy }                   from "passport-http";
import { Strategy as BearerStrategy }      from "passport-http-bearer";
import { Request, Response, NextFunction } from 'express';

let bcrypt = require("bcryptjs");

import { Client, Token, User } from "../models";

/**
 * Used for user authentication.
 */
passport.use(new LocalStrategy({ passReqToCallback: true }, (req, username, password, callback) => {

    User.findOne({ username: username.trim().toLowerCase() }, (err, user) => {
        if (err) {
            return callback(err);
        }

        if (!user) {
            return callback(null, false);
        }

        verifyPassword(user, password, (err, isMatch) => {
            if (err) {
                return callback(err);
            }

            if (!isMatch) {
                return callback(null, false);
            }

            return callback(null, user);
        });
    });
}));

/**
 * Used for client authentication.
 */
passport.use(new BasicStrategy((id, secret, callback) => {

    Client.findOne({ _id: id }, (err, client: any) => {
        if (err) {
            return callback(err);
        }
        if (!client || client.secret !== secret) {
            return callback(null, false);
        }

        return callback(null, client);
    });
}));

/**
 * Used to check if a token is valid.
 */
passport.use(new BearerStrategy((accessToken, callback) => {
    Token.findOne({ value: accessToken }, (err, token: any) => {
        if (err) {
            return callback(err);
        }
        if (!token) {
            return callback(null, false);
        }

        User.findById(token.userId, (err, user) => {
            if (err) {
                return callback(err);
            }
            if (!user) {
                return callback(null, false);
            }

            // Simple example with no scope
            // callback(null, user, { scope: "*" }); // ERROR: missing property "message"
            callback(null, user, { scope: "*", message: "" });
            // callback(null, user);
        });
    });
}));


/**
 *
 * @param user:
 * @param password:
 * @param callback:
 */
function verifyPassword(user, password, callback) {

    bcrypt.compare(password, user.password, (err, isMatch) => {
        if (err) {
            return callback(err);
        }
        callback(null, isMatch);
    });
};

/**
 *
 */
export function isUserAuthenticated(req: Request, res: Response, next: NextFunction) {

    let post_uri: string = `/oauth2/authorize?client_id=${req.query.client_id}&response_type=${req.query.response_type}&redirect_uri=${req.query.redirect_uri}`;

    if (req.body.username) {
        req.body.username = req.body.username.trim().toLowerCase();
    }

    // Register option.
    if (req.body.register) {

        // Check if password and verification password match.
        if (req.body.password !== req.body.verifyPassword) {
            return res.status(409).render("login-register", {
                post_uri: post_uri,
                message: "Password and Verify password do not match."
            });
        }

        // Check if the username has only lowercase, numbers, - or _.
        if (/^([a-z0-9\-_]{1,32})$/.test(req.body.username) === false) {
            return res.status(409).render("login-register", {
                post_uri: post_uri,
                message: "Bad username, use only lowercase, numbers, - or _."
            });
        }

        // Create user.
        let user = new User({
            username: req.body.username,
            email : req.body.email,
            name: req.body.name,
            password: req.body.password,
            public: (req.body.public == "on"),
        });

        // Save user in database.
        user.save((err, user) => {
            if (err) {
                // If exists a user with that username.
                if (err.code == 11000) {
                    return res.status(409).render("login-register", {
                        post_uri: post_uri,
                        message: "This username already exists. Try with other username."
                    });
                }
                // Other error
                else {
                    return res.status(500).render("login-register", {
                        post_uri: post_uri,
                        message: "Error while saving user, try again."
                    });
                }
            }

            // Everything OK
            req.user = user;
            next();
        });
    }
    // Login option.
    else {
        passport.authenticate("local", { session: false }, (err, user, info) => {
            // If an error occured
            if (err) {
                return res.status(500).render("login-register", {
                    post_uri: post_uri,
                    message: "Error while login, try again."
                });
            }
            // If there is no user in the database that match username and password.
            if (!user) {
                return res.status(409).render("login-register", {
                    post_uri: post_uri,
                    message: (!req.body.username) ? "" : "Bad username or password."
                });
            }

            // Everything OK
            req.user = user;
            next();

        })(req, res, next);
    }
}

/**
 *
 */
export let isClientAuthenticated = passport.authenticate("basic",  { session: false });
export let isTokenValid          = passport.authenticate("bearer", { session: false });
