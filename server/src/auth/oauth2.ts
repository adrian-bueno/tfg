//import * as oauth2orize from "oauth2orize";

import { Client, Code, Token, User } from "../models";

let oauth2orize = require("oauth2orize");
let uid = require("uid");

const oauth2server = oauth2orize.createServer();

/**
 * When a client redirects a user to user authorization endpoint, an authorization
 * transaction is initiated. To complete the transaction, the user must authenticate
 * and approve the authorization request. Because this may involve multiple HTTP
 * request/response exchanges, the transaction is stored in the session.
 */

/**
 * Register serialization function.
 */
oauth2server.serializeClient((client, callback) => {
    return callback(null, client._id);
});

/**
 * Register deserialization function.
 */
oauth2server.deserializeClient((id, callback) => {
    Client.findOne({ _id: id }, (err, client) => {
        if (err) {
            return callback(err);
        }

        return callback(null, client);
    });
});

/**
 * Register authorization code grant type.
 *
 * OAuth 2.0 specifies a framework that allows users to grant client applications
 * limited access to their protected resources. It does this through a process
 * of the user granting access, and the client exchanging the grant for an
 * access token.
 *
 * We are registering here for an authorization code grant type. We create a new
 * authorization code model for the user and application client. It is then
 * stored in MongoDB so we can access it later when exchanging for an access
 * token.
 */
oauth2server.grant(oauth2orize.grant.code((client, redirectUri, user, ares, callback) => {
    // Create a new authorization code
    let code: any = new Code({
        value: uid(16),
        redirectUri: redirectUri,
        clientId: client._id,
        userId: user._id
    });

    // Save the auth code and check for errors.
    code.save((err) => {
        if (err) {
            return callback(err);
        }
        callback(null, code.value);
    });
}));

/**
 * Exchange authorization codes for access tokens.
 *
 * What we are doing here is registering for the exchange of authorization codes
 * for access tokens. We first look up to see if we have an authorization code
 * for the one supplied. If we do we perform validation to make sure everything
 * is as it should be. If all is well, we remove the existing authorization code
 * so it cannot be used again and create a new access token. This token is tied
 * to the application client and user. It is finally saved to MongoDB.
 */

oauth2server.exchange(oauth2orize.exchange.code((client, code, redirectUri, callback) => {
    Code.findOne({ value: code }, (err, authCode: any) => {
        if (err) {
            return callback(err);
        }
        if (!authCode) {
            return callback(null, false);
        }
        if (client._id.toString() !== authCode.clientId.toString()) {
            return callback(null, false);
        }
        if (redirectUri !== authCode.redirectUri) {
            return callback(null, false);
        }

        // Delete auth code now that it has been used
        authCode.remove((err) => {
            if (err) {
                return callback(err);
            }

            // Check if a token to this client and user already exists
            Token.findOne({ clientId: authCode.clientId, userId: authCode.userId }, (err, token: any) => {
                if (err) {
                    return callback(err);
                }
                else if (!token) {
                    // No existing token, so we create a new one.
                    let newToken = new Token({
                        value: uid(256),
                        clientId: authCode.clientId,
                        userId: authCode.userId
                    });

                    // Save the new access token.
                    newToken.save((err) => {
                        if (err) {
                            return callback(err);
                        }

                        return callback(null, newToken);
                    });
                }
                else {
                    console.log(token);
                    // Return existing token.
                    return callback(null, token);
                }
            });
        });
    });
}));

/**
 * This endpoint, initializes a new authorization transaction. It finds the
 * client requesting access to the user’s account and then renders the dialog
 * ejs view we created eariler.
 */
let authorization = [
    oauth2server.authorization((clientId, redirectUri, callback) => {
        Client.findById(clientId, (err, client: any) => {
            if (err) {
                return callback(err);
            }
            if (!client) {
                return callback(null, false);
            }
            // if (!client.redirectUri != redirectUri) { // TODO: añadir redirectUri en documento Client
            //     return callback(null, false);
            // }
            return callback(null, client, redirectUri);
        });
    }),
    function (req, res) {
        res.render("dialog", {
            transactionId: req.oauth2.transactionID,
            user: req.user,
            client: req.oauth2.client
        });
    }
];

/**
 * This endpoint is setup to handle when the user either grants or denies access
 * to their account to the requesting application client. The server.decision()
 * function handles the data submitted by the post and will call the
 * server.grant() function we created earlier if the user granted access.
 */
let decision = [
    function (req, res, next) {
        req.user = { _id: req.body.user_id }
        next();
    },
    oauth2server.decision()
];

/*
 * This endpoint is setup to handle the request made by the application client
 * after they have been granted an authorization code by the user. The
 * server.token() function will initiate a call to the server.exchange()
 * function we created earlier.
 */
let token = [
        oauth2server.token(),
        oauth2server.errorHandler()
];

/**
 *
 */
export let oauth2controller = {
    authorization : authorization,
    decision      : decision,
    token         : token
}
