#!/bin/bash

bitrates=()
outputFiles=()
dir=$1
inidir=$PWD

cd $dir
mkdir "dash"

# Get audio bit rate
bit_rate=$(ffprobe -v quiet -print_format json -show_format original.mp3 | grep bit_rate | grep -o '[0-9]\+')

if (( $bit_rate >= 128000 ))
then
    bitrates+=("128k")
    outputFiles+=("128.m4a")
fi

if (( $bit_rate >= 192000 ))
then
    bitrates+=("192k")
    outputFiles+=("192.m4a")
fi

if (( $bit_rate >= 256000 ))
then
    bitrates+=("256k")
    outputFiles+=("256.m4a")
fi

if (( $bit_rate >= 320000 ))
then
    bitrates+=("320k")
    outputFiles+=("320.m4a")
fi

max=${#bitrates[@]}
max=$((max - 1))

for i in `seq 0 ${max}`
do
    echo "[ffmpeg] Converting to" ${outputFiles[$i]}
    ffmpeg -y -loglevel panic -i original.mp3 -vn -c:a aac -strict -2 -b:a ${bitrates[$i]} ${outputFiles[$i]}
done

MP4Box -dash 4000 -frag 4000 -rap -segment-name segment_%s_ -out dash/index ${outputFiles[@]}

# Delete files generated by ffmpeg
for f in ${outputFiles[@]}
do
    rm -f $f
done

cd $inidir
