import * as http    from "http";
import * as https   from "https";
import * as fs      from "fs";
import * as cluster from "cluster";
import * as os      from "os";

import * as global from "./global";
import app         from "./app";

const numCPUs = os.cpus().length;

// Check parameters
// Only one optional parameter is accepted: -https
if (process.argv.length > 3 ||
    (process.argv.length == 3 && process.argv[2] != "-https")) {
    console.log("\x1b[1;31m" + "\nThere is only one optional parameter: -https" + "\x1b[0m");
    process.exit(1);
}

// For HTTPS (argv[2] == "-https")
let options;
if (process.argv.length == 3) {
    options = {
        key: fs.readFileSync("dist/openssl/key.pem"),
        cert: fs.readFileSync("dist/openssl/server.crt")
    }
}

let server;
const port = normalizePort(process.env.PORT || global.port);

if (cluster.isMaster) {
    console.log(`Master ${process.pid} is running`);

    // Fork workers
    for (let i = 0; i < numCPUs; i++) {
        cluster.fork();
    }

    cluster.on("exit", (worker, code, signal) => {
        console.log(`Worker ${worker.process.pid} died`);
    });
} else {
    // Workers can share any TCP connection
    // In this case it is an HTTP server

    if (options)
        server = https.createServer(options, app);
    else
        server = http.createServer(app);

    server.listen(port);

    // Event functions
    server.on('error', onError);
    server.on('listening', onListening);

    // http.createServer(app).listen(port);
    console.log(`Worker ${process.pid} started`);
}

/**
 *
 */
function normalizePort(val: number | string): number | string | boolean {
    let port: number = (typeof val === 'string') ? parseInt(val, 10) : val;
    if (isNaN(port)) return val;
    else if (port >= 0) return port;
    else return false;
}

/**
 *
 */
function onError(error: NodeJS.ErrnoException): void {

    if (error.syscall !== 'listen') throw error;
    let bind = (typeof port === 'string') ? 'Pipe ' + port : 'Port ' + port;

    switch (error.code) {
        case 'EACCES':
            console.error(`${bind} requires elevated privileges`);
            process.exit(1);
            break;
        case 'EADDRINUSE':
            console.error(`${bind} is already in use`);
            process.exit(1);
            break;
        default:
            throw error;
    }
}

/**
 *
 */
function onListening(): void {
    let addr = server.address();
    let bind = (typeof addr === 'string')
        ? `pipe ${addr}`
        : `port ${addr.port}`;

    console.log(`Listening on ${bind}`);
}
