import * as path       from 'path';
import * as express    from 'express';
import * as session    from 'express-session';
import * as logger     from 'morgan';
import * as bodyParser from 'body-parser';
import * as ejs        from 'ejs';
import * as ConnectRedis from 'connect-redis';
let RedisStore = ConnectRedis(session);

import {
    AllRouter,
    ClientsRouter,
    MeRouter,
    OAuth2Router,
    PlaylistRouter,
    SearchRouter,
    StreamRouter,
    TracksRouter,
    UsersRouter
} from "./routes";

/**
 * Creates and configures an ExpressJS web server.
 */
class App {

    // Ref. to Express instance
    express: express.Application;

    /**
     * Run configuration methods on the Express instance.
     */
    constructor() {
        this.express = express();
        this.middleware();
        this.routes();
    }

    /**
     * Configure Express middleware.
     */
    private middleware(): void {
        this.express.use(logger('dev'));
        this.express.use(bodyParser.json());
        this.express.use(bodyParser.urlencoded({ extended: true }));

        // Static files
        this.express.use(express.static(__dirname + '/public'));
        this.express.use("/images", express.static(path.join(__dirname, "../storage/images")));
        this.express.use("/audio", express.static(path.join(__dirname, "../storage/audio")));

        // View Engine
        this.express.set("views", path.join(__dirname, "views"));
        this.express.set("view engine", "ejs");
        this.express.engine("ejs", ejs.renderFile);

        // Use express session support since OAuth2orize requires it
        // Use Redis to store sessions (necessary if using processes cluster)
        this.express.use(session({
            store: new RedisStore({ttl: 300}), // max session live = 5 min
            secret: "Super Secret Session Key"
        }));

        // Headers, allow CORS
        this.express.use((req, res, next) => {
            res.header("Access-Control-Allow-Origin", "*");
            res.header("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE");
            res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Authorization");
            next();
        });
    }

    /**
     * Configure API endpoints.
     */
    private routes(): void {

        let RootRouter = express.Router();
        RootRouter.get('/', (req, res, next) => {
            res.json({
                message: 'Welcome to our API!',
                pid: process.pid
            });
        });

        this.express.use('/'         , RootRouter);
        this.express.use('/all'      , AllRouter);
        this.express.use('/clients'  , ClientsRouter);
        this.express.use('/me'       , MeRouter);
        this.express.use('/oauth2'   , OAuth2Router);
        this.express.use('/playlists', PlaylistRouter);
        this.express.use('/search'   , SearchRouter);
        this.express.use('/stream'   , StreamRouter);
        this.express.use('/tracks'   , TracksRouter);
        this.express.use('/users'    , UsersRouter);
    }

}

export default new App().express;
