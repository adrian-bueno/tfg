import { Schema, Types, model } from 'mongoose';

let TokenSchema = new Schema({
    userId      : { type: Schema.Types.ObjectId, ref: 'User', required: true },
    clientId    : { type: Schema.Types.ObjectId, ref: 'Client', required: true },
    value       : { type: String, required: true } // TODO: hash
});

/**
 *
 */
TokenSchema.set('toJSON', {
    transform: function (doc, ret, options) {
        delete ret._id;
        delete ret.__v;
        delete ret.userId;
        delete ret.clientId;
    }
});

export let Token = model("Token", TokenSchema);
