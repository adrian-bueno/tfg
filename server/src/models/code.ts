import { Schema, Types, model } from 'mongoose';

let CodeSchema = new Schema({
    value       : { type: String, required: true }, // TODO: hash
    redirectUri : { type: String, required: true },
    userId      : { type: Schema.Types.ObjectId, ref: 'User', required: true },
    clientId    : { type: Schema.Types.ObjectId, ref: 'Client', required: true }
});

export let Code = model("Code", CodeSchema);
