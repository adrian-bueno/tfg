import { Schema, Types, model } from 'mongoose';
import { baseUrl }              from "../global";

let PlaylistSchema = new Schema({
    createdDate  : { type: Date, default: Date.now },
    description  : String,
    genres       : [String],
    image        : { type: {
                       dominantColor : String,
                       textColor     : String,
                       url           : String,
                       qualities     : [Number],
                       format        : String
                   }, default: {dominantColor: "#323232", textColor:"#eee", url: "images/default/no-cover.jpg", format: "jpg" }},
    isAlbum      : { type: Boolean, default: false },
    labels       : [String],
    name         : { type: String, required: true },
    public       : { type: Boolean, default: true },
    owner        : { type: Schema.Types.ObjectId, ref: 'User', required: true },
    releaseDate  : Date,
    tracks       : { type: [{
                       addedAt : { type: Date, default: Date.now },
                       addedBy : { type: Schema.Types.ObjectId, ref: 'User', required: true },
                       id      : { type: Schema.Types.ObjectId, ref: 'Track', required: true }
                   }]}
});

/**
 *
 */
PlaylistSchema.set('toJSON', {
    transform: function (doc, ret, options) {
        ret.id = ret._id;
        if (ret.image)
            ret.image.url = baseUrl + ret.image.url;
        ret.type = "playlist";
        delete ret._id;
        delete ret.__v;
    }
});

export let Playlist = model("Playlist", PlaylistSchema);
