import { Schema, Types, model } from 'mongoose';

let ClientSchema = new Schema({
    name       : { type: String, required: true, unique: true },
    secret     : { type: String, required: true },
    userId     : { type: Schema.Types.ObjectId, ref: 'User', required: true },
});

ClientSchema.set('toJSON', {
    transform: function (doc, ret, options) {
        ret.id = ret._id;
        delete ret._id;
        delete ret.__v;
    }
});

export let Client = model("Client", ClientSchema);
