import { Schema, Types, model } from 'mongoose';
import { baseUrl }              from "../global";
let bcrypt = require("bcryptjs");

/**
 *
 */
let UserSchema: any = new Schema({
    username        : { type: String, unique: true, required: true },
    email           : { type: String, required: true },
    profileImage    : { type: String, default: "images/default/no-profile.jpg" },
    backgroundImage : { type: String, default: "images/default/no-background.jpg" },
    name            : { type: String, required: true },
    password        : { type: String, required: true },
    public          : { type: Boolean, default: true },
    country         : String,
    description     : String,
});

/**
 *
 */
UserSchema.set('toJSON', {
    transform: function (doc, ret, options) {
        ret.id = ret._id;
        ret.type = "user";
        if (ret.profileImage) ret.profileImage = baseUrl + ret.profileImage;
        if (ret.backgroundImage) ret.backgroundImage = baseUrl + ret.backgroundImage;

        delete ret._id;
        delete ret.__v;
        delete ret.password;
    }
});

/**
 * Execute before each User.save() call
 */
UserSchema.pre("save", function(callback) {

    let user = this;

    // Break out if the password hasn't changed
    if (!user.isModified("password")) {
        return callback();
    }

    // Password changed so we need to hash it
    bcrypt.hash(user.password, 5, (err, hash) => {
        if (err) {
            return callback(err);
        }

        user.password = hash;
        callback();
    });
});


export let User = model("User", UserSchema);
