import { Schema, Types, model } from "mongoose";
import { baseUrl }              from "../global";

let TrackSchema = new Schema({
    albumId     : { type: Schema.Types.ObjectId, ref: 'Playlist' }, // Id to playlist that is an album
    artists     : { type: [Schema.Types.ObjectId], ref: 'User' }, // artists with an account in Bluzu
    artistsN    : [String], // artists without an account in Bluzu
    createdDate : { type: Date, default: Date.now },
    duration    : { type: Number, default: 0 }, // in seconds
    explicit    : { type: Boolean, default: false },
    genres      : [String],
    isPlayable  : { type: Boolean, default: false },
    image        : { type: {
                       dominantColor : String,
                       textColor     : String,
                       url           : String,
                       qualities     : [Number],
                       format        : String
                   }, default: {dominantColor: "#323232", textColor:"#eee", url: "images/default/no-cover.jpg", format: "jpg" }},
    name        : { type: String },
    owner       : { type: Schema.Types.ObjectId, ref: 'User', required: true },
    public      : { type: Boolean, default: true },
    releaseDate : Date,
    // url         : { type: String, default: "no audio file uploaded yet" },
    url         : { type: {
                      html5 : String,
                      dash  : String
                  }},
    trackNumber : Number,
});

/**
 *
 */
TrackSchema.set('toJSON', {
     transform: function (doc, ret, options) {
         ret.id = ret._id;
         ret.type = "track";

         if (ret.image)
            ret.image.url = baseUrl + ret.image.url;

         if (ret.isPlayable == true && ret.url !== undefined) {
            ret.url.html5 = baseUrl + ret.url.html5;
            ret.url.dash = baseUrl + ret.url.dash;
        }

        delete ret._id;
        delete ret.__v;
     }
});


export let Track = model("Track", TrackSchema);
