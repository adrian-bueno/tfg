import { Router, Request, Response, NextFunction } from "express";
import * as path   from "path";
import * as fs     from "fs";
// import * as mediaserver from "mediaserver";
let mediaserver = require("mediaserver");

import { audioStoragePath } from "../global";
import { Playlist, Track, User } from "../models";

class StreamRouterConfig {

    router: Router;

    /**
     * Initialize the AudioRouter
     */
    constructor() {
        this.router = Router();
        this.routes();
    }

    /**
     * Take each handler, and attach to one of the Express.Router's
     * endpoints.
     */
    routes() {
        this.router.get('/:id/dash/:file', this.streamDash);
        this.router.get('/:id/html5', this.streamHtml5);
    }

    /**
     * TODO: check if track is public, if not, only send to owner
     */
    streamHtml5(req: Request, res: Response, next: NextFunction) {

        // console.log("[[[ mediaserver ]]]");

        let filePath: string = `${audioStoragePath}/${req.params.id}/original.mp3`;

        fs.stat(filePath, (err, stat) => {
            if (err) {
                return res.status(404).json({ err: "File does not exist." });
            }

            mediaserver.pipe(req, res, filePath);
        });

    }

    /**
     * TODO: check if track is public, if not, only send to owner
     */
    streamDash(req: Request, res: Response, next: NextFunction) {

        // console.log("[[[ DASH ]]]");

        let filePath: string = path.join(audioStoragePath, req.params.id, "dash", req.params.file);

        fs.stat(filePath, (err, stat) => {
            if (err) {
                return res.status(404).json({ err: "File does not exist." });
            }

            let readStream = fs.createReadStream(filePath);

            readStream.pipe(res);
        });
    }

}

// Create the StreamRouter, and export its configured Express.Router
export let StreamRouter = new StreamRouterConfig().router;
