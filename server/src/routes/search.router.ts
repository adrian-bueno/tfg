import { Router, Request, Response, NextFunction } from "express";
import { Types } from "mongoose";

import { Playlist, Track, User } from "../models";

class SearchRouterConfig {

    router: Router;

    /**
     * Initialize the SearchRouter
     */
    constructor() {
        this.router = Router();
        this.routes();
    }

    /**
     * Take each handler, and attach to one of the Express.Router's
     * endpoints.
     */
    routes() {
        this.router.get('/playlists/:str', this.searchPlaylists);
        this.router.get('/users/:str', this.searchUsers);
        this.router.get('/tracks/:str', this.searchTracks);
    }

    /**
     * TODO: dont get private playlists
     * Limit number of results
     */
    searchPlaylists(req: Request, res: Response, next: NextFunction) {
        Playlist.find({ name: new RegExp(req.params.str.trim(), 'i') }, (err, playlists) => {
            if (err)
                res.status(500).json({ message: "Error getting playlists." });
            else
                res.status(200).json(playlists);
        });
    }

    /**
     * TODO: dont get private users
     * Limit number of results
     */
    searchUsers(req: Request, res: Response, next: NextFunction) {

        let str: string = req.params.str.trim();

        // Search by username
        if (str[0] == "@") {
            str = str.replace(/@/g, ''); // remove all '@'

            User.find({ username: new RegExp(str, 'i') }, (err, users) => {
                if (err)
                    res.status(500).json({ message: "Error getting users." });
                else
                    res.status(200).json(users);
            });
        }
        // Search by name
        else {
            User.find({ name: new RegExp(str, 'i') }, (err, users) => {
                if (err)
                    res.status(500).json({ message: "Error getting users." });
                else
                    res.status(200).json(users);
            });
        }
    }

    /**
     * TODO: dont get private tracks
     * Limit number of results
     */
    searchTracks(req: Request, res: Response, next: NextFunction) {
        Track.find({ name: new RegExp(req.params.str.trim(), 'i') }, (err, tracks) => {
            if (err)
                res.status(500).json({ message: "Error gettting tracks." });
            else
                res.status(200).json(tracks);
        });
    }

}

// Create the SearchRouter, and export its configured Express.Router
export let SearchRouter = new SearchRouterConfig().router;
