import { Router, Request, Response, NextFunction } from "express";
import { Types } from "mongoose";

import { Track } from "../models";

class TracksRouterConfig {

    router: Router;

    /**
     * Initialize the AlbumsRouter
     */
    constructor() {
        this.router = Router();
        this.routes();
    }

    /**
     * Take each handler, and attach to one of the Express.Router's
     * endpoints.
     */
    routes() {
        this.router.get('/', this.getMultiple); // ?ids=12345...,23426...,63464....
        this.router.get('/:id', this.getOne);
    }

    /**
     * TODO: dont get private tracks
     */
    getMultiple(req: Request, res: Response, next: NextFunction) {

        let ids: string[] = [];
        let regex: any = /[a-z0-9]{24}/g;
        let maxIds: number = 20;
        let id: any;

        for (let i = 0; i < maxIds; i++) {
            id = regex.exec(req.query.ids);
            if (!id) break;
            else ids.push(id[0]);
        }

        Track.find( { _id: { $in: ids.map(Types.ObjectId) } }, (err, tracks) => {
            if (err)
                res.status(500).json({ message: "Error finding tracks." });
            else
                res.status(200).json(tracks);
        });
    }

    /**
     * TODO: dont get private tracks
     */
    getOne(req: Request, res: Response, next: NextFunction) {

        Track.findOne({ _id: req.params.id }, (err, track) => {
            if (err) {
                res.status(500).json({ message: "An error occurred while searching this track." });
            }
            else if (!track) {
                res.status(404).json({ message: "Cannot find a track with that id." });
            }
            else {
                res.status(200).json(track);
            }
        });
    }

}

// Create the AlbumsRouter, and export its configured Express.Router
export let TracksRouter = new TracksRouterConfig().router;
