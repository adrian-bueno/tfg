import { Router, Request, Response, NextFunction } from "express";
import { Types } from "mongoose";

import { Playlist, Track } from "../models";
import { isTokenValid } from "../auth";

class PlaylistRouterConfig {

    router: Router;

    /**
     * Initialize the AlbumsRouter
     */
    constructor() {
        this.router = Router();
        this.routes();
    }

    /**
     * Take each handler, and attach to one of the Express.Router's
     * endpoints.
     */
    routes() {
        this.router.get('/', this.getMultiple); // ?ids=12345...,23426...,63464....
        this.router.get('/:id', this.getOne);
        this.router.get('/:id/tracks', this.getPlaylistTracks);
    }

    /**
     * TODO: dont get private playlists
     */
    getMultiple(req: Request, res: Response, next: NextFunction) {

        let ids: string[] = [];
        let regex: any = /[a-z0-9]{24}/g;
        let maxIds: number = 20;
        let id: any;

        for (let i = 0; i < maxIds; i++) {
            id = regex.exec(req.query.ids);
            if (!id) break;
            else ids.push(id[0]);
        }

        Playlist.find( { _id: { $in: ids.map(Types.ObjectId) } }, (err, playlists) => {
            if (err)
                res.status(500).json({ message: "Error finding playlists." });
            else
                res.status(200).json(playlists);
        });
    }

    /**
     * TODO: dont get private playlists
     */
    getOne(req: Request, res: Response, next: NextFunction) {

        Playlist.findById(req.params.id, (err, playlist) => {
            if (err) {
                res.status(500).json({ message: "An error occurred while searching this playlist." });
            }
            else if (!playlist) {
                res.status(404).json({ message: "Cannot find an playlist with that id." });
            }
            else {
                res.status(200).json(playlist);
            }
        }).populate("owner", "username name"); //.populate("tracks.id");
    }

    /**
     * TODO: dont get private playlists/tracks
     */
    getPlaylistTracks(req: Request, res: Response, next: NextFunction) {

        Playlist.findById(req.params.id, (err, playlist: any) => {
            if (err) {
                res.status(500).json({ message: "An error occurred while searching this playlist." });
            }
            else if (!playlist) {
                res.status(404).json({ message: "Cannot find a playlist with that id." });
            }
            else {
                let listOfIds: string[] = [];
                for (let track of playlist.tracks) {
                    listOfIds.push(track.id);
                }
                Track.find({ _id: { $in: listOfIds } }, (err, tracks) => {
                    if (err) {
                        res.status(500).json({ message: "An error occurred while getting playlist tracks." });
                    }
                    else {
                        res.status(200).json(tracks);
                    }
                }).populate("owner", "username name");
            }
        });
    }

}

// Create the AlbumsRouter, and export its configured Express.Router
export let PlaylistRouter = new PlaylistRouterConfig().router;
