import { Router, Request, Response, NextFunction } from "express";
import { Types } from "mongoose";

import { Playlist, Track, User } from "../models";

class AllRouterConfig {

    router: Router;

    constructor() {
        this.router = Router();
        this.routes();
    }

    routes() {
        this.router.get('/playlists', this.getAllPlaylists);
        this.router.get('/users', this.getAllUsers);
        this.router.get('/tracks', this.getAllTracks);
    }

     getAllPlaylists(req: Request, res: Response, next: NextFunction) {
         Playlist.find((err, playlists) => {
             if (err)
                 res.status(500).json({ message: "Error getting playlists." });
             else
                 res.status(200).json(playlists);
         }).populate("owner", "username name");
     }

    getAllUsers(req: Request, res: Response, next: NextFunction) {
        User.find((err, users) => {
            if (err)
                res.status(500).json({ message: "Error getting users." });
            else
                res.status(200).json(users);
        });
    }

    getAllTracks(req: Request, res: Response, next: NextFunction) {
        Track.find((err, tracks) => {
            if (err)
                res.status(500).json({ message: "Error gettting tracks." });
            else
                res.status(200).json(tracks);
        }).populate("owner", "username name");;
    }

}

export let AllRouter = new AllRouterConfig().router;
