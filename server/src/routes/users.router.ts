import { Router, Request, Response, NextFunction } from 'express';
import { Types }   from "mongoose";
import * as multer from "multer";
import * as path   from "path";
import * as fs     from "fs";

import { Playlist, Track, User } from "../models";
import { isTokenValid }       from "../auth";
import { imageStoragePath }   from "../global";


class UsersRouterConfig {

    router: Router;

    /**
     * Initialize the UsersRouter
     */
    constructor() {
        this.router = Router();
        this.routes();
    }

    /**
     * Take each handler, and attach to one of the Express.Router's
     * endpoints.
     */
    routes(): void {
        this.router.get('/', this.getMultiple); // ?ids=12345...,23426...,63464....
        this.router.get('/:id', this.getOne);
        this.router.get('/:id/playlists', this.getUserPlaylists);
        this.router.get('/:id/tracks', this.getUserTracks);

        // TODO:
        // this.router.get('/:id/favorites', isTokenValid, this.getUserPlaylists);
        // this.router.get('/:id/reposts', isTokenValid, this.getUserPlaylists);
        // this.router.get('/:id/following', isTokenValid, this.getUserPlaylists);
        // this.router.get('/:id/followers', isTokenValid, this.getUserPlaylists);
    }

    /**
     *
     */
    getMultiple(req: Request, res: Response, next: NextFunction) {

        let ids: string[] = [];
        let regex: any = /[a-z0-9]{24}/g;
        let maxIds: number = 20;
        let id: any;

        for (let i = 0; i < maxIds; i++) {
            id = regex.exec(req.query.ids);
            if (!id) break;
            else ids.push(id[0]);
        }

        User.find( { _id: { $in: ids.map(Types.ObjectId) } }, (err, users) => {
            if (err)
                res.status(500).json({ message: "Error finding users." });
            else
                res.status(200).json(users);
        });
    }

    /**
     *
     */
    getOne(req: Request, res: Response, next: NextFunction): void {

        User.findOne({ _id: req.params.id }, (err, user: any) => {
            if (err) {
                res.status(500).json({message: "An error occurred while searching this user."});
            }
            else if (!user) {
                res.status(404).json({message: "Cannot find a user with that account name."});
            }
            else {
                res.status(200).json(user);
            }
        });
    }

    /**
     *
     */
    getUserPlaylists(req: Request, res: Response, next: NextFunction) {

        User.findOne({ _id: req.params.id }, (err, user: any) => {
            if (err) {
                res.status(500).json({ message: "An error occurred while searching this user." });
            }
            else if (!user) {
                res.status(404).json({ message: "Cannot find a user with that id." });
            }
            else {
                Playlist.find({ owner: user._id }, (err, playlists) => {
                    if (err) {
                        res.status(500).json({ message: "An error occurred while getting user playlists." });
                    }
                    else {
                        res.status(200).json(playlists);
                    }
                });
            }
        });
    }

    /**
     *
     */
    getUserTracks(req: Request, res: Response, next: NextFunction) {

        User.findOne({ _id: req.params.id }, (err, user: any) => {
            if (err) {
                res.status(500).json({ message: "An error occurred while searching this user." });
            }
            else if (!user) {
                res.status(404).json({ message: "Cannot find a user with that id." });
            }
            else {
                Track.find({ owner: user._id }, (err, tracks) => {
                    if (err) {
                        res.status(500).json({ message: "An error occurred while getting user tracks." });
                    }
                    else {
                        res.status(200).json(tracks);
                    }
                });
            }
        });
    }

}

// Create the UsersRouter, and export its configured Express.Router
export let UsersRouter = new UsersRouterConfig().router;
