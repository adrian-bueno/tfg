import { Router, Request, Response, NextFunction } from 'express';
import * as mongoose from 'mongoose';

import {
    isUserAuthenticated,
    isClientAuthenticated,
    oauth2controller
} from "../auth";


class OAuth2RouterConfig {

    router: Router;

    /**
     * 
     */
    constructor() {
        this.router = Router();
        this.routes();
    }

    /**
     * Take each handler, and attach to one of the Express.Router's
     * endpoints.
     */
    routes(): void {
        this.router.get('/authorize', isUserAuthenticated, oauth2controller.authorization);
        this.router.post('/authorize', isUserAuthenticated, oauth2controller.authorization);
        this.router.post('/decision', oauth2controller.decision);
        this.router.post('/token', isClientAuthenticated, oauth2controller.token);
    }

}

// Create the OAuth2Router, and export its configured Express.Router
export let OAuth2Router = new OAuth2RouterConfig().router;
