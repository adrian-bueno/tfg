import { Router, Request, Response, NextFunction } from 'express';
import * as multer from "multer";
import * as path   from "path";
import * as fs     from "fs-extra";
let Jimp = require("jimp");
let exec = require("child_process").exec;
let sharp = require('sharp');
let mp3Duration = require('mp3-duration');

import { isTokenValid } from "../auth";
import { Playlist, Track, User } from "../models";
import { audioStoragePath, imageStoragePath, baseUrl, dashScript } from "../global";
import { ImageUtils } from "../other";

/**
 * TODO
 * Bug: when multer Unexpected field, multer deletes previous file whos name has the id passed.
 */

class MeRouterConfig {

    router: Router;
    storage: any;
    uploadAudio: any;
    uploadImages: any;

    /**
     * Initialize the MeRouter
     */
    constructor() {
        //
        this.storage = multer.diskStorage({
            destination: function(req: any, file, cb) {
                if (file.fieldname == "trackAudio") {
                    let dir = path.join(audioStoragePath, req.params.id);
                    fs.mkdirs(dir, (err) => cb(err, dir));
                    // cb(null, path.join(audioStoragePath, req.params.id));
                }
                else {
                    // cb(null, path.join(imageStoragePath));
                    let id: string = (req.params.id != undefined) ? req.params.id : req.user._id.toString();
                    let dir = path.join(imageStoragePath, id);
                    fs.mkdirs(dir, (err) => cb(err, dir));
                }
            },
            filename: function(req: any, file, cb) {
                // if (file.fieldname == "profileImage")
                //     cb(null, req.body.id + '_profile.' + file.mimetype.split("/")[1]);
                // else if (file.fieldname == "backgroundImage")
                //     cb(null, req.body.id + '_background.' + file.mimetype.split("/")[1]);
                // else if (file.fieldname == "trackAudio")
                //     cb(null, 'original.' + file.mimetype.split("/")[1]);
                // else // playlist image and track image and audio files
                //     cb(null, req.params.id + '.' + file.mimetype.split("/")[1]);

                let mimetype: string = file.mimetype.split("/")[1];

                if (file.fieldname == "profileImage")
                    // cb(null, req.body.id + '_profile.' + file.mimetype.split("/")[1]);
                    cb(null, `original_profile.${mimetype}`);
                else if (file.fieldname == "backgroundImage")
                    // cb(null, req.body.id + '_background.' + file.mimetype.split("/")[1]);
                    cb(null, `original_background.${mimetype}`);
                // else if (file.fieldname == "trackAudio")
                    // cb(null, 'original.' + file.mimetype.split("/")[1]);
                else // playlist image and track image and audio files
                    // cb(null, req.params.id + '.' + file.mimetype.split("/")[1]);
                    cb(null, `original.${mimetype}`);
            }
        });

        //
        this.uploadAudio = multer({ storage: this.storage, fileFilter: this.fileFilter, limits: { fileSize: 314600000 } }); // max audio size = 300 MB
        this.uploadImages = multer({ storage: this.storage, fileFilter: this.fileFilter, limits: { fileSize: 2100000 } });  // max image size = 2 MB

        //
        this.router = Router();
        this.routes();
    }

    /**
     * Take each handler, and attach to one of the Express.Router's
     * endpoints.
     */
    routes(): void {
        this.router.get('/', isTokenValid, this.getUserInfo);
        this.router.put('/', isTokenValid, this.updateUserInfo);

        this.router.post('/upload', isTokenValid, this.uploadImages.fields([{ name: "profileImage", maxCount: 1 }, { name: "backgroundImage", maxCount: 1 }]), this.multerErrorHandler, this.uploadUserImages);

        this.router.get('/playlists', isTokenValid, this.getPlaylists);
        this.router.post('/playlist', isTokenValid, this.newPlaylist);
        this.router.put('/playlist/:id', isTokenValid, this.updatePlaylist);
        this.router.put('/playlist/:id/add-track', isTokenValid, this.addTrackToPlaylist);
        this.router.post('/playlist/:id/uploadImage', /*isTokenValid,*/ this.uploadImages.single("playlistImage"), this.multerErrorHandler, this.uploadPlaylistImage);
        this.router.delete('/playlist/:id', isTokenValid, this.deletePlaylist);

        this.router.get('/tracks', isTokenValid, this.getTracks);
        this.router.post('/track', isTokenValid, this.newTrack);
        this.router.put('/track/:id', isTokenValid, this.updateTrack);
        this.router.post('/track/:id/uploadAudio', /*isTokenValid,*/ this.uploadAudio.single("trackAudio"), this.multerErrorHandler, this.uploadTrackAudio);
        this.router.post('/track/:id/uploadImage', /*isTokenValid,*/ this.uploadImages.single("trackImage"), this.multerErrorHandler, this.uploadTrackImage);
        this.router.delete('/track/:id', isTokenValid, this.deleteTrack);

        // TODO:
        // this.router.get('/favorites', isTokenValid, this.getUserPlaylists);
        // this.router.get('/reposts', isTokenValid, this.getUserPlaylists);
        // this.router.get('/following', isTokenValid, this.getUserPlaylists);
        // this.router.get('/followers', isTokenValid, this.getUserPlaylists);
    }

    /**
     * TODO: check if is owner
     */
    private fileFilter(req, file, cb): void {

        // Check if audio is not in mp3 format.
        if (file.fieldname == "trackAudio" && file.mimetype != "audio/mp3") {
            let error: any = new Error("Bad Format");
            error.code = "BAD_FILE_TYPE"
            return cb(error);
        }
        // Check if images are not in jpg or png formats.
        if (file.fieldname != "trackAudio" && file.mimetype != "image/jpg" && file.mimetype != "image/png" && file.mimetype != "image/jpeg") {
            let error: any = new Error("Bad Format");
            error.code = "BAD_FILE_TYPE"
            return cb(error);
        }

        // If formats are valid:

        // If audio then check if track exists and user is owner
        if (file.fieldname == "trackAudio" || file.fieldname == "trackImage") {
            Track.findOne({ _id: req.params.id }, (err, track) => {
                // Track.findOne({ _id: req.body.id, owner: req.user._id }, (err, track) => { // TODO: check owner
                if (err) {
                    let error: any = new Error("An error occurred while searching track.");
                    error.code = "FIND_ERROR";
                    return cb(error);
                }
                if (!track) {
                    let error: any = new Error("Bad id or not the owner.");
                    error.code = "BAD_ID_OR_OWNER";
                    return cb(error);
                }
                else {
                    req.track = track;
                    return cb(null, true);
                }
            });
        }
        // If playlistImage then check if playlist exists and user is owner
        else if (file.fieldname == "playlistImage") {
            Playlist.findOne({ _id: req.params.id }, (err, playlist) => {
                // Playlist.findOne({ _id: req.body.id, owner: req.user._id }, (err, playlist) => { // TODO: check owner
                if (err) {
                    let error: any = new Error("An error occurred while searching playlist.");
                    error.code = "FIND_ERROR";
                    return cb(error);
                }
                if (!playlist) {
                    let error: any = new Error("Bad id or not the owner.");
                    error.code = "BAD_ID_OR_OWNER";
                    return cb(error);
                }
                else {
                    req.playlist = playlist;
                    return cb(null, true);
                }
            });
        }
        // profileImage and backgroundImage
        else {
            // We don't need to do anything.
            // user is already in req do to bearer strategy.
            return cb(null, true);
        }
    }

    /**
     * Get error from multer
     * This function is only executed when an error happens.
     */
    multerErrorHandler(err: any, req: any, res: Response, next: NextFunction) {
        if (err.code === "BAD_FILE_TYPE")
            return res.status(400).json({ message: "Bad file type, only .mp3, .jpg/.jpeg and .png are accepted." });
        if (err.code === "BAD_ID_OR_OWNER")
            return res.status(400).json({ message: "Bad id or not the owner." });
        if (err.code === "FIND_ERROR")
            return res.status(500).json({ message: "An error occurred while uploading files." });
        if (err.code === "LIMIT_FILE_SIZE")
            return res.status(400).json({ message: "Max file size exceeded, audio files can be up to 300 MB and images up to 2 MB." });
        if (err.code === "LIMIT_UNEXPECTED_FILE")
            return res.status(400).json({ message: "Unexpected field." });

        return res.status(500).json({ message: "Error uploading files.", error: err });
    }

    /**
     *
     */
    getUserInfo(req: Request, res: Response, next: NextFunction): void {

        User.findById(req.user._id, (err, user) => {
            if (err) {
                res.status(500).json({ message: "Error finding user." });
            }
            if (!user) {
                res.status(404).json({ message: "Cannot find user." });
            }
            else {
                res.status(200).json(user);
            }
        });
    }

    /**
     * TODO:
     * Añadir otros campos a actualizar
     */
    private updateUserInfo(req: Request, res: Response, next: NextFunction) {

        if (req.user && req.body.name)
            req.user.name = req.body.name;

        if (req.user && req.body.public !== undefined)
            req.user.public = req.body.public;

        req.user.save((err, updatedUser) => {
            if (err) {
                res.status(500).json({ message: "Error updating user data." });
            }
            else {
                res.status(200).json({
                    message: "User updated correctly.",
                    user: updatedUser
                });
            }
        });
    }

    /**
     *
     */
    private newPlaylist(req: Request, res: Response, next: NextFunction) {

        let playlist = new Playlist({
            description: req.body.description,
            genres: req.body.genres,
            isAlbum: req.body.isAlbum,
            labels: req.body.labels,
            name: req.body.name,
            public: req.body.public,
            owner: req.user._id,
            // releaseDate : req.body.releaseDate // TODO: convert to UTC
        });

        playlist.save((err, playlist) => {
            if (err) {
                res.status(500).json({ message: "Error creating playlist." });
            }
            else {
                res.status(200).json({
                    message: "Playlist created correctly.",
                    playlist: playlist
                });
            }
        });
    }

    /**
     * TODO sin terminar
     */
    private updatePlaylist(req: Request, res: Response, next: NextFunction) {

        Playlist.findOne({ _id: req.params.id, owner: req.user._id }, function(err, playlist: any) {
            if (err) {
                res.status(500).json({ message: "Error updating track data." });
            }
            else if (!playlist) {
                res.status(400).json({ message: "Incorrect id or not the owner." });
            }
            else {
                // atributes to update
                if (req.body.playlistType) playlist.playlistType = req.body.playlistType;
                if (req.body.genres) playlist.genres = req.body.genres;
                if (req.body.labels) playlist.labels = req.body.labels;
                if (req.body.name) playlist.name = req.body.name;
                if (req.body.releaseDate) playlist.releaseDate = req.body.releaseDate;
                if (req.body.public != undefined) playlist.public = req.body.public;

                playlist.save(function(err, updatedPlaylist) {
                    if (err) {
                        res.status(500).json({ message: "An error occurred while saving updated playlist data." });
                    }
                    else {
                        res.status(200).json({
                            message: "Playlist correctly updated.",
                            playlist: updatedPlaylist
                        });
                    }
                });
            }
        });
    }

    /**
     *
     */
    private newTrack(req: Request, res: Response, next: NextFunction) {

        let track = new Track({
            albumId: req.body.albumId, // Playlist with isAlbum = true
            artists: req.body.artists,
            artistsN: req.body.artistsN,
            explicit: req.body.explicit,
            genres: req.body.genres,
            name: req.body.name,
            owner: req.user._id,
            // releaseDate : req.body.releaseDate, // TODO: transform to UTC
            trackNumber: req.body.trackName,
            public: req.body.public
        });

        track.save((err, track) => {
            if (err) {
                res.status(500).json({
                    message: "Error while saving track info.",
                    error: err
                });
            }
            else {
                res.status(200).json({
                    message: "Track correctly saved.",
                    track: track
                });
            }
        });
    }

    /**
     * TODO
     */
    private updateTrack(req: Request, res: Response, next: NextFunction) {

        Track.findOne({ _id: req.params.id, owner: req.user._id }, function(err, track: any) {
            if (err) {
                res.status(500).json({ message: "Error updating track data." });
            }
            else if (!track) {
                res.status(400).json({ message: "Incorrect id or not the owner." });
            }
            else {
                // atributes to update
                if (req.body.albumId) track.albumId = req.body.albumId;
                if (req.body.artists) track.artists = req.body.artists;
                if (req.body.artistsN) track.artistsN = req.body.artistsN;
                if (req.body.explicit) track.explicit = req.body.explicit;
                if (req.body.genres) track.genres = req.body.genres;
                if (req.body.isPlayable) track.isPlayable = req.body.isPlayable;
                if (req.body.name) track.name = req.body.name;
                if (req.body.trackNumber) track.trackNumber = req.body.trackNumber;
                if (req.body.public != undefined) track.public = req.body.public;

                track.save(function(err, updatedTrack) {
                    if (err) {
                        res.status(500).json({ message: "An error occurred while saving updated track data." });
                    }
                    else {
                        res.status(200).json({
                            message: "Track correctly updated.",
                            track: updatedTrack
                        });
                    }
                });
            }
        });
    }

    /**
     *
     */
    private deleteTrack(req: Request, res: Response, next: NextFunction) {

        Track.findOne({ _id: req.params.id, owner: req.user._id }, (err, track: any) => {
            if (err) {
                res.status(500).json({ message: "Error deleting track." });
            }
            else if (!track) {
                res.status(400).json({ message: "Incorrect id or not the owner." });
            }
            else {
                Track.remove({ _id: req.params.id }, (err) => {
                    if (err) {
                        res.status(500).json({ message: "The track could not be deleted do to an error, try again." });
                    }
                    else {
                        // Delete audio files
                        if (track.url !== undefined) {
                            let trackAudioDir: string = path.join(audioStoragePath, track.id);
                            // Remove audio files directory using fs-extra (it works like 'rm -rf' command)
                            fs.remove(trackAudioDir, (err) => {
                                if (err) {
                                    console.log("ERROR DELETING TRACK AUDIO DIRECTORY - track.id = " + track.id);
                                    console.log(err);
                                }
                            });
                        }

                        // Delete image file
                        if (track.image.url !== "images/default/no-cover.jpg") {
                            let imageFilePath: string = path.join(imageStoragePath, track.id);
                            fs.remove(imageFilePath, (err) => {
                                if (err) {
                                    console.log("ERROR DELETING TRACK IMAGE - track.id = " + track.id);
                                    console.log(err);
                                }
                            });
                        }

                        res.status(200).json({ message: "Track deleted" });
                    }
                });
            }
        });
    }

    /**
     *
     */
    private getPlaylists(req: Request, res: Response, next: NextFunction) {

        let chooseProperties: "";

        if (req.query.select)
            chooseProperties = req.query.select;

        Playlist.find({ owner: req.user._id }, chooseProperties, (err, playlists) => {
            if (err) {
                res.status(500).json({ message: "Error getting playlists." });
            }
            else {
                res.status(200).json({
                    message: "User playlists",
                    playlists: playlists
                });
            }
        });
    }

    /**
     *
     */
    private getTracks(req: Request, res: Response, next: NextFunction) {

        let chooseProperties: "";

        if (req.query.select)
            chooseProperties = req.query.select;

        Track.find({ owner: req.user._id }, chooseProperties, (err, tracks) => {
            if (err) {
                res.status(500).json({ message: "Error getting tracks." });
            }
            else {
                res.status(200).json({
                    message: "User tracks",
                    tracks: tracks
                });
            }
        });
    }

    /**
     *
     */
    private deletePlaylist(req: Request, res: Response, next: NextFunction) {

        Playlist.findOne({ _id: req.params.id, owner: req.user._id }, (err, playlist: any) => {
            if (err) {
                res.status(500).json({ message: "Error deleting track." });
            }
            else if (!playlist) {
                res.status(400).json({ message: "Incorrect id or not the owner." });
            }
            else {
                // Delete image files
                if (playlist.image) {
                    let playlistImagesDir: string = path.join(imageStoragePath, playlist.id);
                    // Remove audio files directory using fs-extra (it works like 'rm -rf' command)
                    fs.remove(playlistImagesDir, (err) => {
                        if (err) {
                            console.log("ERROR DELETING PLAYLIST IMAGE DIRECTORY - playlist.id = " + playlist.id);
                            console.log(err);
                        }
                    });
                }

                Playlist.remove({ _id: req.params.id }, (err) => {
                    if (err)
                        res.status(500).json({ message: "The playlist could not be deleted do to an error, try again." });
                    else
                        res.status(200).json({ message: "Playlist deleted" });
                });
            }
        });
    }

    /**
     * TODO
     */
    private addTrackToPlaylist(req: Request, res: Response, next: NextFunction) {

        Track.findById(req.body.trackId, (err, track: any) => {
            if (err)
                return res.status(500).json({ message: "Error adding track to playlist. (1)" });
            else if (!track)
                return res.status(400).json({ message: "Incorrect track id." });

            if (track.public === false && track.owner.toString() !== req.user._id.toString())
                return res.status(400).json({ message: "Private track and not the owner of it." });

            // Playlist.findByIdAndUpdate(
            //     req.params.id,
            Playlist.findOneAndUpdate(
                { _id: req.params.id, 'tracks.id': { $ne: req.body.trackId } },
                {
                    $push: {
                        "tracks": {
                            addedBy: req.user._id,
                            id: req.body.trackId
                        }
                    }
                },
                { new: true },
                (err, playlist) => {
                    if (err)
                        return res.status(500).json({ message: "Error adding track to playlist. (2)" });
                    if (!playlist)
                        return res.status(400).json({ message: "Incorrect playlist id or track already added to it." });

                    return res.status(200).json({
                        message: "Track correctly added to playlist.",
                        playlist: playlist
                    });
                }
            );
        });
    }

    /**
     * TODO
     */
    private removeTrackFromPlaylist(req: Request, res: Response, next: NextFunction) {

        Playlist.findOne({ _id: req.params.id, owner: req.user._id }, (err, playlist: any) => {
            if (err) {
                res.status(500).json({ message: "Error removing track from playlist." });
            }
            else if (!playlist) {
                res.status(400).json({ message: "Incorrect id or not the owner." });
            }
            else {
                Playlist.remove({ _id: req.params.id }, (err) => {
                    if (err)
                        res.status(500).json({ message: "The playlist could not be deleted do to an error, try again." });
                    else
                        res.status(200).json({ message: "Playlist deleted" });
                });
            }
        });
    }

    /**
     * Param. "req" is of type Request, but it is put to any because we add a new atributte: "error"
     * Capturar el error que lanza multer cuando se sube un fichero con un nombre que no esta en la lista,
     * seguramente esto se tiene que hacer en otro lado, no en esta funcion.
     * TODO: no hace falta buscar al usuario, ya se a buscado al comprobar el token, esta en req.user
     */
    private uploadUserImages(req: any, res: Response, next: NextFunction) {

        // // No uploaded images, so req.files = {} (empty object)
        if (Boolean(req.files && typeof req.files == 'object') && !Object.keys(req.files).length) {
            return res.status(400).json({ message: "No files uploaded." });
        }
        // Files correctly uploaded
        // User.findById(req.params.id, (err, user: any) => {
        User.findById(req.user._id, (err, user: any) => {
            if (err) {
                res.status(500).json({
                    message: "Error updating user images. (1)",
                    error: err
                });
            }
            else {
                user.profileImage = (req.files.profileImage)
                    ? "images/" + req.user._id + "/original_profile." + req.files.profileImage[0].mimetype.split("/")[1]
                    : user.profileImage;

                user.backgroundImage = (req.files.backgroundImage)
                    ? "images/" + req.user._id + "/original_background." + req.files.backgroundImage[0].mimetype.split("/")[1]
                    : user.backgroundImage;

                user.save((err, user) => {
                    if (err) {
                        res.status(500).json({
                            message: "Error updating user images. (2)",
                            error: err
                        });
                    }
                    else {
                        res.status(200).json({
                            message: "Files uploaded.",
                            user: user
                        });
                    }
                });
            }
        });
    }


    /**
     * TODO: check if is owner of the playlist
     */
    private uploadPlaylistImage(req: any, res: Response, next: NextFunction) {

        // No uploaded image
        if (req.file === undefined) {
            return res.status(400).json({ message: "No file uploaded." });
        }

        // let imagePath: string = imageStoragePath + req.params.id + "." + req.file.mimetype.split("/")[1];
        let imagePath: string = `${imageStoragePath}/${req.params.id}/original.${req.file.mimetype.split("/")[1]}`;

        // Image correctly uploaded.
        Playlist.findById(req.params.id, (err, playlist: any) => { // TODO: check if is owner of the playlist
            if (err) {
                // Delete image file
                fs.unlink(imagePath, (err) => {
                    if (err) console.log(err);
                });

                res.status(500).json({ message: "Error finding playlist." });
            }
            if (!playlist) {
                // Delete image file
                fs.unlink(imagePath, (err) => {
                    if (err) console.log(err);
                });

                res.status(404).json({ message: "There is no playlist with that id." });
            }
            else {
                // let imagePath: string = imageStoragePath + req.params.id + "." + req.file.mimetype.split("/")[1];
                // let imagePath: string = `${imageStoragePath}/${req.params.id}/${req.params.id}.${req.file.mimetype.split("/")[1]}`;
                Jimp.read(imagePath, (err, image) => {
                    if (err) {
                        return res.status(500).json({ message: "Error updating playlist image url. (1)", error: err });
                    }

                    let colors: string[] = ImageUtils.getDominantColor(image);
                    image = undefined;

                    // Continue processing image (different qualities)
                    ImageUtils.processImage(`${imageStoragePath}/${req.params.id}`, 'original', req.file.mimetype.split("/")[1], (err, qualities) => {
                        // console.log(qualities);

                        playlist.image = {
                            dominantColor: colors[0],
                            textColor: colors[1],
                            url: `images/${req.params.id}`,
                            qualities: qualities,
                            format: req.file.mimetype.split("/")[1]
                            // url: "images/" + req.params.id + "." + req.file.mimetype.split("/")[1]
                        };

                        playlist.save((err, playlist) => {
                            if (err) {
                                return res.status(500).json({ message: "Error updating playlist image url. (2)", error: err });
                            }
                            else {
                                return res.status(200).json({
                                    message: "Playlist image correctly uploaded.",
                                    playlist: playlist
                                });
                            }
                        });
                    });
                });
            }
        });
    }

    /**
     * TODO:
     */
    private uploadTrackImage(req: any, res: Response, next: NextFunction) {

        // No uploaded image
        if (req.file === undefined) {
            return res.status(400).json({ message: "No file uploaded." });
        }

        // let imagePath: string = imageStoragePath + req.params.id + "." + req.file.mimetype.split("/")[1];
        let imagePath: string = `${imageStoragePath}/${req.params.id}/original.${req.file.mimetype.split("/")[1]}`;

        // Image correctly uploaded.
        Track.findById(req.params.id, (err, track: any) => { // TODO: check if is owner of the playlist
            if (err) {
                // Delete image file // TODO cambiar a nueva forma
                fs.unlink(imagePath, (err) => {
                    if (err) console.log(err);
                });

                res.status(500).json({ message: "Error finding track." });
            }
            if (!track) {
                // Delete image file // TODO cambiar a nueva forma
                fs.unlink(imagePath, (err) => {
                    if (err) console.log(err);
                });

                res.status(404).json({ message: "There is no track with that id." });
            }
            else {
                // ImageUtils.processImage(`${imageStoragePath}/${req.params.id}`, 'original', req.file.mimetype.split("/")[1], (err, qualities) => {
                //     track.image = {
                //         dominantColor: track.image.dominantColor,
                //         textColor: track.image.textColor,
                //         url: `images/${req.params.id}`,
                //         qualities: qualities,
                //         format: req.file.mimetype.split("/")[1]
                //     };
                //
                //     track.save((err, track) => {
                //         if (err) {
                //             return res.status(500).json({ message: "Error updating track image url.", error: err });
                //         }
                //         else {
                //             return res.status(200).json({
                //                 message: "Track image correctly uploaded.",
                //                 track: track
                //             });
                //         }
                //     });
                // });
                Jimp.read(imagePath, (err, image) => {
                    if (err) {
                        return res.status(500).json({ message: "Error updating track image. (1)", error: err });
                    }

                    let colors: string[] = ImageUtils.getDominantColor(image);
                    image = undefined;

                    // Continue processing image (different qualities)
                    ImageUtils.processImage(`${imageStoragePath}/${req.params.id}`, 'original', req.file.mimetype.split("/")[1], (err, qualities) => {
                        // console.log(qualities);

                        track.image = {
                            dominantColor: colors[0],
                            textColor: colors[1],
                            url: `images/${req.params.id}`,
                            qualities: qualities,
                            format: req.file.mimetype.split("/")[1]
                            // url: "images/" + req.params.id + "." + req.file.mimetype.split("/")[1]
                        };

                        track.save((err, track) => {
                            if (err) {
                                return res.status(500).json({ message: "Error updating track image. (2)", error: err });
                            }
                            else {
                                return res.status(200).json({
                                    message: "Track image correctly uploaded.",
                                    track: track
                                });
                            }
                        });
                    });
                }); // Jimp
            }
        });

    }

    /**
     * TODO: comprobar como controlar errores de la mejor forma, para no tener
     * que hacer el apaño que he hecho.
     * Param. "req" is of type Request, but it is put to any because we add a new atributte: "error"
     */
    private uploadTrackAudio(req: any, res: Response, next: NextFunction) {

        if (!req.file) {
            res.status(400).json({ message: "No file uploaded." });
        }
        else { // File correctly uploaded
            let filePath: string = path.join(audioStoragePath, req.params.id, "original.mp3");
            let dir: string = path.join(audioStoragePath, req.params.id);

            // Execute bash script that creates dash files
            exec(`${dashScript} ${dir}`, (err, stdout, stderr) => {
                // console.log("------------");
                // console.log(err);
                // console.log("------------");
                // console.log(stdout);
                // console.log("------------");
                // console.log(stderr);
                // console.log("------------");
            });

            mp3Duration(filePath, (err, duration) => {
                if (err) {
                    // Delete audio file
                    fs.unlink(filePath, (err) => {
                        if (err) console.log(err);
                    });

                    res.status(500).json({
                        message: "Error updating track data. (1)"
                    });
                }
                else {
                    //
                    Track.findByIdAndUpdate(req.params.id,
                        {
                            $set:
                            {
                                isPlayable: true,
                                // url: "stream/" + req.params.id + "/html5",
                                url: {
                                    html5: "stream/" + req.params.id + "/html5",
                                    dash: "stream/" + req.params.id + "/dash/index.mpd"
                                },
                                duration: duration
                            }
                        },
                        { new: true }, (err, track) => {

                            if (err) {
                                // Delete audio file // TODO cambiar por la forma nueva
                                fs.unlink(filePath, (err) => {
                                    if (err) console.log(err);
                                });

                                res.status(500).json({
                                    message: "Error updating track data. (2)"
                                });
                            }
                            else {
                                res.status(200).json({
                                    message: "File uploaded.",
                                    track: track
                                });
                            }
                        }); // Track.findByIdAndUpdate
                }
            }); // mp3Duration
        }
    }

}

// Create the MeRouter, and export its configured Express.Router
export let MeRouter = new MeRouterConfig().router;
