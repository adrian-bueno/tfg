import { Router, Request, Response, NextFunction } from 'express';
import * as mongoose from 'mongoose';
import * as uid from 'uid';

import { Client } from "../models";
import { isTokenValid } from "../auth";


class ClientsRouterConfig {

    router: Router;

    constructor() {
        this.router = Router();
        this.routes();
    }

    routes(): void {
        this.router.get('/', /*isTokenValid,*/ this.getAllClients);
        this.router.post('/', /*isTokenValid,*/ this.postOneClient);
    }

    /**
     * Get all clients from one authenticated user.
     */
    getAllClients(req: Request, res: Response, next: NextFunction): void {

        // Client.find( { userId: req.user._id }, (err, clients) => {
        Client.find((err, clients) => {
            if (err) {
                res.status(404).json({
                    message: "Cannot find that client.",
                    error: err
                });
            }
            else {
                res.status(200).json(clients);
            }
        });
    }

    /**
     * Creates a new client for an authenticated user.
     */
    postOneClient(req: Request, res: Response, next: NextFunction): void {

        let client = new Client({
            name   : req.body.name,
            secret : uid(128),
            userId : req.body.userId
            // userId : req.user._id
        });

        client.save((err, client) => {
            if (err) {
                res.status(500).json({
                    message: "Error while saving client.",
                    error: err
                });
            }
            else {
                res.status(200).json({ message: "Client saved correctly.", data: client });
            }
        });
    }

}

// Create the ClientsRouter, and export its configured Express.Router
export let ClientsRouter = new ClientsRouterConfig().router;
