/**
 * Global values.
 */

import { databaseConnect } from "./database";
import * as path           from "path";

// Server
export const ip      : string = "127.0.0.1";
export const port    : number = 8080;
export const baseUrl : string = `http://${ip}:${port}/`;

// Database
export const dbUrl : string = "mongodb://127.0.0.1/bluzu";
export const db    : any    = databaseConnect(dbUrl);

// Storage location
export const audioStoragePath : string = path.join(__dirname, "../storage/audio/");
export const imageStoragePath : string = path.join(__dirname, "../storage/images/");

// Dash script location
export const dashScript : string = path.join(__dirname, "generate-dash.sh");
