module.exports = function(grunt) {
    "use strict";

    grunt.initConfig({
        copy: {
            build: {
                files: [
                    {
                        expand: true,
                        cwd: "./src/public",
                        src: ["**"],
                        dest: "./dist/public"
                    },
                    {
                        expand: true,
                        cwd: "./src/views",
                        src: ["**"],
                        dest: "./dist/views"
                    },
                    {
                        expand: true,
                        cwd: "./src/openssl",
                        src: ["**"],
                        dest: "./dist/openssl"
                    },
                    {
                        expand: true,
                        cwd: "./src",
                        src: "generate-dash.sh",
                        dest: "./dist"
                    }
                ]
            }
        },
        watch: {
            views: {
                files: ["src/views/**/*"],
                tasks: ["copy"]
            },
            css: {
                files: ["src/public/css/**/*"],
                tasks: ["copy"]
            }
        }
    });

    grunt.loadNpmTasks("grunt-contrib-copy");
    // grunt.loadNpmTasks("grunt-contrib-watch");

    grunt.registerTask("default", [
        "copy",
        // "watch"
    ]);

};
