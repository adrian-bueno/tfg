# Build app for production and use in a browser
rm -rf dist
npm run build-prod
rm -rf aot
cp -r src/images dist/images
cp -r src/css dist/css
cp src/favicon.ico dist
cp src/manifest.json dist
cp src/index-aot.html dist/index.html
