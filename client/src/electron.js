const { app, BrowserWindow, globalShortcut, ipcMain } = require('electron');
const path = require('path');
const url = require('url');

// Keep a global reference of the window object, if you don't, the window will
// be closed automatically when the JavaScript object is garbage collected.
let window;
let authWindow;

function createWindow() {
    // Create the browser window.
    window = new BrowserWindow({
        width: 1200,
        height: 800,
        backgroundColor: '#212121',
        // frame: false
    });

    // window.webContents.clearHistory();

    // and load the index.html of the app.
    window.loadURL(url.format({
        pathname: path.join(__dirname, 'index-electron.html'),
        protocol: 'file:',
        slashes: true
    }));

    // Open the DevTools.
    // window.webContents.openDevTools();

    // Emitted when the window is closed.
    window.on('closed', () => {
        // Dereference the window object, usually you would store windows
        // in an array if your app supports multi windows, this is the time
        // when you should delete the corresponding element.
        window = null;
    });
};

// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
// Some APIs can only be used after this event occurs.
app.on('ready', () => {
    createWindow();

    globalShortcut.register('MediaPlayPause', () => {
        window.webContents.send('global-shortcut', 'MediaPlayPause');
    });

    globalShortcut.register('MediaNextTrack', () => {
        window.webContents.send('global-shortcut', 'MediaNextTrack');
    });

    globalShortcut.register('MediaPreviousTrack', () => {
        window.webContents.send('global-shortcut', 'MediaPreviousTrack');
    });

});

// Quit when all windows are closed.
app.on('window-all-closed', () => {

    globalShortcut.unregisterAll();

    // On macOS it is common for applications and their menu bar
    // to stay active until the user quits explicitly with Cmd + Q
    if (process.platform !== 'darwin') {
        app.quit();
    };
});

app.on('activate', () => {
    // On macOS it's common to re-create a window in the app when the
    // dock icon is clicked and there are no other windows open.
    if (window === null) {
        createWindow();
    }
});


// Login - authWindow
ipcMain.on('login', (event, arg) => {

    authWindow = new BrowserWindow({
        width: 800,
        height: 725,
        backgroundColor: '#212121',
        frame: false
    });

    authWindow.loadURL(arg);
    authWindow.show();

    // Handle the response

    authWindow.webContents.on('will-navigate', function(event, url) {
        window.webContents.send('logged', handleCallback(url));
    });

    authWindow.webContents.on('did-get-redirect-request', function(event, oldUrl, newUrl) {
        window.webContents.send('logged', handleCallback(newUrl));
    });

    // Reset the authWindow on close
    authWindow.on('close', () => {
        authWindow = null;
    });
});

function handleCallback(url) {
    var raw_code = /code=([^&]*)/.exec(url) || null;
    var code = (raw_code && raw_code.length > 1) ? raw_code[1] : null;
    var error = /\?error=(.+)$/.exec(url);

    // Close the browser if code found or error
    if (code || error)
        authWindow.destroy();

    // If there is a code, proceed to get token from github
    if (code)
        return code;
    else if (error)
        return "error";
}
