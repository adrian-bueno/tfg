/**
 * System configuration for Angular 2 samples
 * Adjust as necessary for your application needs.
 */
(function(global) {

    let nodeDir = "node_modules/";
    let electron = "@empty";
    // If executed in node (using electron)
    if (typeof process === 'object' && process + '' === '[object process]') {
        nodeDir = "../node_modules/";
        electron = "@node/electron";
    }

    System.config({
        paths: {
            // paths serve as alias
            'npm:': nodeDir
        },
        // map tells the System loader where to look for things
        map: {
            // our app is within the app folder
            app: 'app',

            // components
            components: 'app/components',
            home: 'app/components/home',
            trackinqueue: 'app/components/play-queue/track-in-queue',
            playqueue: 'app/components/play-queue',
            player: 'app/components/player',
            playerbig: 'app/components/player/player-big',
            playlist: 'app/components/playlist',
            search: 'app/components/search',
            shared: 'app/components/shared',
            contextmenu: 'app/components/shared/context-menu',
            forms: 'app/components/shared/forms',
            editplaylistform: 'app/components/shared/forms/edit-playlist-form',
            box: 'app/components/shared/box',
            sidenav: 'app/components/sidenav',
            sidenavnologged: 'app/components/sidenav/sidenav-no-logged',
            sidenavoption: 'app/components/sidenav/sidenav-option',
            upload: 'app/components/upload',
            uploadtrack: 'app/components/upload/upload-track',
            user: 'app/components/user',
            userhome: 'app/components/user/user-home',
            usermusic: 'app/components/user/user-music',
            userplaylists: 'app/components/user/user-playlists',
            usersimilarartists: 'app/components/user/user-similar-artists',
            userabout: 'app/components/user/user-about',

            // object models
            models: 'app/models',

            // services
            services: 'app/services',

            // angular bundles
            '@angular/core': 'npm:@angular/core/bundles/core.umd.js',
            '@angular/common': 'npm:@angular/common/bundles/common.umd.js',
            '@angular/compiler': 'npm:@angular/compiler/bundles/compiler.umd.js',
            '@angular/platform-browser': 'npm:@angular/platform-browser/bundles/platform-browser.umd.js',
            '@angular/platform-browser-dynamic': 'npm:@angular/platform-browser-dynamic/bundles/platform-browser-dynamic.umd.js',
            '@angular/http': 'npm:@angular/http/bundles/http.umd.js',
            '@angular/router': 'npm:@angular/router/bundles/router.umd.js',
            '@angular/forms': 'npm:@angular/forms/bundles/forms.umd.js',

            // other libraries
            'rxjs': 'npm:rxjs',
            'angular2-in-memory-web-api': 'npm:angular2-in-memory-web-api',

            // node libraries
            'electron': electron
        },
        // packages tells the System loader how to load when no filename and/or no extension
        packages: {
            app: {
                main: './main.js',
                defaultExtension: 'js'
            },
            rxjs: {
                defaultExtension: 'js'
            },
            'angular2-in-memory-web-api': {
                main: './index.js',
                defaultExtension: 'js'
            },
            components: {
                main: './index.js',
                defaultExtension: 'js'
            },
            home: {
                main: './index.js',
                defaultExtension: 'js'
            },
            playqueue: {
                main: './index.js',
                defaultExtension: 'js'
            },
            playlist: {
                main: './index.js',
                defaultExtension: 'js'
            },
            trackinqueue: {
                main: './index.js',
                defaultExtension: 'js'
            },
            player: {
                main: './index.js',
                defaultExtension: 'js'
            },
            playerbig: {
                main: './index.js',
                defaultExtension: 'js'
            },
            search: {
                main: './index.js',
                defaultExtension: 'js'
            },
            shared: {
                main: './index.js',
                defaultExtension: 'js'
            },
            contextmenu: {
                main: './index.js',
                defaultExtension: 'js'
            },
            forms: {
                main: './index.js',
                defaultExtension: 'js'
            },
            editplaylistform: {
                main: './index.js',
                defaultExtension: 'js'
            },
            box: {
                main: './index.js',
                defaultExtension: 'js'
            },
            sidenav: {
                main: './index.js',
                defaultExtension: 'js'
            },
            sidenavnologged: {
                main: './index.js',
                defaultExtension: 'js'
            },
            sidenavoption: {
                main: './index.js',
                defaultExtension: 'js'
            },
            upload: {
                main: './index.js',
                defaultExtension: 'js'
            },
            uploadtrack: {
                main: './index.js',
                defaultExtension: 'js'
            },
            user: {
                main: './index.js',
                defaultExtension: 'js'
            },
            userhome: {
                main: './index.js',
                defaultExtension: 'js'
            },
            usermusic: {
                main: './index.js',
                defaultExtension: 'js'
            },
            userplaylists: {
                main: './index.js',
                defaultExtension: 'js'
            },
            usersimilarartists: {
                main: './index.js',
                defaultExtension: 'js'
            },
            userabout: {
                main: './index.js',
                defaultExtension: 'js'
            },
            models: {
                main: './index.js',
                defaultExtension: 'js'
            },
            services: {
                main: './index.js',
                defaultExtension: 'js'
            }
        }
    });
})(this);
