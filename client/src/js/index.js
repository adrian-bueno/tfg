function isExecInNode() {
    if (typeof process === 'object' && process + '' === '[object process]') {
        return true;
    }
    return false;
}

function setJQueryForNode() {
    if (isExecInNode()) {
        window.$ = window.jQuery = module.exports;
    }
}

if (!isExecInNode())
    document.getElementById("base").href = "/";
