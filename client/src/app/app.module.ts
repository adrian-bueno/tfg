import { NgModule }      from '@angular/core';
import { BrowserModule, HammerGestureConfig, HAMMER_GESTURE_CONFIG } from '@angular/platform-browser';
import { FormsModule }   from '@angular/forms';
import { HttpModule }    from '@angular/http';

import { AppComponent }  from './app.component';
import { AppRoutingModule } from './app.routing';
import {
    SearchComponent,
    SidenavComponent,
    SidenavNoLoggedComponent,
    SidenavOptionComponent,
    PlayQueueComponent,
    PlayerComponent,
    PlayerBigComponent,
    PlaylistComponent,
    HomeComponent,
    UploadComponent,
    UploadTrackComponent,
    UserComponent,
    UserHomeComponent,
    UserMusicComponent,
    UserPlaylistsComponent,
    UserSimilarArtistsComponent,
    UserAboutComponent,
    BoxComponent,
    ContextMenuComponent,
    EditPlaylistFormComponent,
    EditPlaylistFormService
} from './components';

import { BluzuService, GlobalService, PlayerService } from './services';

/**
 * Hammer touch events custom configuration
 */
export class HammerConfig extends HammerGestureConfig {
    overrides = <any> {
        'swipe': { direction: Hammer.DIRECTION_ALL, velocity: 0.3, threshold: 10 }, // override default settings
        'pan': { direction: Hammer.DIRECTION_ALL, velocity: 0.3, threshold: 10 }
    }
}

@NgModule({
    imports: [
        BrowserModule,
        FormsModule,
        HttpModule,
        AppRoutingModule
    ],
    declarations: [
        AppComponent,
        SearchComponent,
        SidenavComponent,
        SidenavNoLoggedComponent,
        SidenavOptionComponent,
        PlayQueueComponent,
        PlayerComponent,
        PlayerBigComponent,
        PlaylistComponent,
        HomeComponent,
        UploadComponent,
        UploadTrackComponent,
        UserComponent,
        UserHomeComponent,
        UserMusicComponent,
        UserPlaylistsComponent,
        UserSimilarArtistsComponent,
        UserAboutComponent,
        BoxComponent,
        ContextMenuComponent,
        EditPlaylistFormComponent,
    ],
    providers: [
        BluzuService,
        GlobalService,
        PlayerService,
        EditPlaylistFormService,
        {
            provide: HAMMER_GESTURE_CONFIG,
            useClass: HammerConfig
        }
    ],
    bootstrap: [AppComponent]
})

export class AppModule { }
