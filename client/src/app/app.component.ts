import { Component, OnInit, Output, EventEmitter, AfterViewInit } from '@angular/core';

import { BluzuService, GlobalService } from './services';
import { Track, User, Playlist } from './models';
import { EditPlaylistFormService } from './components';

declare var $: any;
declare var Hammer: any;

@Component({
    moduleId: module.id,
    selector: 'app',
    templateUrl: 'app.component.html',
    styleUrls: ['app.component.css']
})

export class AppComponent implements OnInit, AfterViewInit {

    maxMobileWidth: number = 745;

    leftDivIsOpen: boolean  = true;
    rightDivIsOpen: boolean = false;
    centerDivIsSmall: boolean = false;
    footerDivHeight = "60px";
    footerDivTransition = "0.5s";
    footerDivIsFullHeight = false;
    logged: boolean = false;
    runningOnMobile: boolean = false;

    contextMenuTop: number = 0;
    contextMenuLeft: number = 0;
    contextMenuUser: User;
    contextMenuTrack: Track;
    contextMenuPlaylist: Playlist;

    // Used to calculate size of footer-div on pan
    lastMouseYPos: number = 0;

    constructor(private bluzuService: BluzuService, private globalService: GlobalService,
            private editPlaylistFormService: EditPlaylistFormService) {

        if (globalService.runningOnMobile()) {
            this.runningOnMobile = true;
        }
    }

    ngOnInit() {

        // Check if a user is logged
        if (!this.bluzuService.getLoggedUser())
            this.logged = false;
        else
            this.logged = true;

        // Subscribe to services
        this.bluzuService.logEmitter$.subscribe(user => {
            if (!user)
                this.logged = false;
            else
                this.logged = true;
        });

        this.globalService.contextMenu$.subscribe(data => {
            this.contextMenuTop = data.top;
            this.contextMenuLeft = data.left;
            this.contextMenuUser = data.user;
            this.contextMenuTrack = data.track;
            this.contextMenuPlaylist = data.playlist;
        });

        if (window.innerWidth <= this.maxMobileWidth) {
            this.leftDivIsOpen = false;
        }

        this.globalService.broadcastNewContentWidth(document.getElementsByClassName("center-div")[0].clientWidth);
    }

    ngAfterViewInit() {
        this.centerDivIsSmall = this.checkCenterDivIsSmall();
        if (this.centerDivIsSmall) {
            this.globalService.broadcastNewContentWidth(document.getElementsByClassName("center-div")[0].clientWidth);
            setTimeout(() => {
                this.globalService.broadcastNewContentWidth(document.getElementsByClassName("center-div")[0].clientWidth);
            }, 100);
        }
    }

    onWindowResize(event: any) {
        this.globalService.broadcastNewContentWidth(document.getElementsByClassName("center-div")[0].clientWidth);
    }

    footerDivOnPan(event) {
        this.footerDivHeight = `${$(".footer-div")[0].offsetHeight + (this.lastMouseYPos - event.srcEvent.clientY)}px`;
        this.lastMouseYPos = event.srcEvent.clientY;
    }

    footerDivOnPanStart(event) {
        this.footerDivTransition = "0s";
        this.lastMouseYPos = event.srcEvent.clientY;
    }

    footerDivOnPanEnd(event) {
        this.footerDivTransition = "0.5s";
        if ($(".footer-div").height() > window.innerHeight / 2) {
            this.footerDivHeight = "100%";
            this.footerDivIsFullHeight = true;
        }
        else {
            this.footerDivHeight = "60px";
            this.footerDivIsFullHeight = false;
        }
    }

    footerDivSwipeUp() {
        this.footerDivHeight = "100%";
        this.footerDivIsFullHeight = true;
    }

    footerDivSwipeDown() {
        this.footerDivHeight = "60px";
        this.footerDivIsFullHeight = false;
    }

    leftDivButtonClicked(): void {

        let centerDiv: any = document.getElementsByClassName("center-div")[0];

        this.centerDivIsSmall = this.checkCenterDivIsSmall();

        if (this.leftDivIsOpen == true) {
            if (!this.centerDivIsSmall)
                this.globalService.broadcastNewContentWidth(centerDiv.clientWidth + 250);
            else
                this.globalService.broadcastNewContentWidth(centerDiv.clientWidth);
            this.leftDivIsOpen = false;
        }
        else {
            if (!this.centerDivIsSmall)
                this.globalService.broadcastNewContentWidth(centerDiv.clientWidth - 250);
            else
                this.globalService.broadcastNewContentWidth(centerDiv.clientWidth);
            this.leftDivIsOpen = true;
        }

        if (!this.centerDivIsSmall) {
            // Wait 300ms because menu open/close animation lasts 300ms
            setTimeout(() => {
                this.globalService.broadcastNewContentWidth(centerDiv.clientWidth);
            }, 300);
        }
    }

    rightDivButtonClicked(): void {

        let centerDiv: any = document.getElementsByClassName("center-div")[0];

        this.centerDivIsSmall = this.checkCenterDivIsSmall();

        if (this.rightDivIsOpen == true) {
            if (!this.centerDivIsSmall)
                this.globalService.broadcastNewContentWidth(centerDiv.clientWidth + 250);
            else
                this.globalService.broadcastNewContentWidth(centerDiv.clientWidth);
            this.rightDivIsOpen = false;
        }
        else {
            if (!this.centerDivIsSmall)
                this.globalService.broadcastNewContentWidth(centerDiv.clientWidth - 250);
            else
                this.globalService.broadcastNewContentWidth(centerDiv.clientWidth);
            this.rightDivIsOpen = true;
        }

        if (!this.centerDivIsSmall) {
            // Wait 300ms because menu open/close animation lasts 300ms
            setTimeout(() => {
                this.globalService.broadcastNewContentWidth(centerDiv.clientWidth);
            }, 300);
        }
    }

    checkCenterDivIsSmall(): boolean {
        // if (document.getElementsByClassName("center-div")[0].clientWidth < 745)
        if (window.innerWidth < this.maxMobileWidth)
            return true;
        return false;
    }

    contextMenu(event: MouseEvent) {
        event.preventDefault();
    }

    playlistModalCancel(): void {
        this.editPlaylistFormService.cancel();
    }


    playlistModalCreate(): void {
        this.editPlaylistFormService.create();
    }

}
