import { Component, OnInit, OnChanges, Input, SimpleChanges } from '@angular/core';

import { Playlist, Track, User } from '../../../models';
import { BluzuService, PlayerService } from '../../../services';

declare var $: any;


@Component({
    moduleId: module.id,
    selector: 'context-menu',
    templateUrl: 'context-menu.component.html',
    styleUrls: ['context-menu.component.css']
})

export class ContextMenuComponent implements OnInit, OnChanges {

    @Input() top: number;
    @Input() left: number;
    @Input() track: Track;
    @Input() user: User;
    @Input() playlist: Playlist;

    loggedUser: User;
    mePlaylists: Playlist[] = [];

    itsMine: boolean = false;
    display: string = "none";
    mainMenuTop = "";
    mainMenuLeft = "";
    playlistsMenuTop = "";
    playlistsMenuLeft = "";
    closeMenu: boolean = false;

    confirmedDeletion: boolean = false;
    deleteColor: string = "";
    deleteText: string = "Delete";

    /**
     *
     */
    constructor(private bluzuService: BluzuService, private playerService: PlayerService) {

    }

    /**
     *
     */
    ngOnInit() {
        this.display = "none";

        this.loggedUser = this.bluzuService.getLoggedUser();
        if (this.loggedUser) {
            this.bluzuService.getMePlaylists("name")
                .subscribe(playlists => this.mePlaylists = playlists);
        }

        this.bluzuService.logEmitter$.subscribe(user => {
            this.loggedUser = user;
            if (this.loggedUser) {
                this.bluzuService.getMePlaylists("name")
                    .subscribe(playlists => this.mePlaylists = playlists);
            }
        });

        this.bluzuService.newPlaylist$.subscribe((playlist: Playlist) => {
            this.mePlaylists.push(playlist);
        });
    }

    /**
     *
     */
    ngOnChanges(changes: SimpleChanges) {

        // console.log(changes);

        this.display = "block";
        this.mainMenuTop = this.top + "px";
        this.mainMenuLeft = this.left + "px";

        // Check if main-menu has overflow, if it has, then modify the top and left values

        // If I dont use jQuery to change display property the following code is executed before the context-menu is display:block
        $("#_context-menu").css("display", "block");

        // let mainMenuOverflowY = $("#main-menu").innerHeight() - (window.innerHeight - changes['top'].currentValue);
        let mainMenuOverflowY = $("#main-menu").innerHeight() - (window.innerHeight - this.top);

        if (mainMenuOverflowY > 0) {
            // this.mainMenuTop = (changes['top'].currentValue - mainMenuOverflowY - 10) + "px";
            this.mainMenuTop = (this.top - mainMenuOverflowY - 10) + "px";
        }

        this.itsMine = false;

        if (this.user !== undefined) {
            // do nothing for the moment
        }
        else if (this.track && this.loggedUser) {
            if (this.track.owner.id === this.loggedUser.id)
                this.itsMine = true;
        }
        else if (this.playlist && this.loggedUser) {
            if (this.playlist.owner.id === this.loggedUser.id)
                this.itsMine = true;
        }

    }

    /**
     *
     */
    showPlaylistsMenu(event: MouseEvent) {
        let playlistsListOverflowY = $("#playlists-list").innerHeight() - (window.innerHeight - $("#playlists-list").offset().top);

        if (playlistsListOverflowY > 0) {
            this.playlistsMenuTop = (-35 - playlistsListOverflowY - 10) + "px";
        }
    }

    /**
     *
     */
    resetPlaylistsMenu(event: MouseEvent) {
        this.playlistsMenuTop = -35 + "px";
    }

    /**
     *
     */
    closeContextMenu() {
        if (this.closeMenu) {
            this.display = "none";
            this.confirmedDeletion = false;
            this.deleteColor = "";
            this.deleteText = "Delete";
        }
        this.closeMenu = true;
    }

    /**
     *
     */
    addTrackToPlaylist(playlistId: string) {
        this.bluzuService.addTrackToPlaylist(this.track.id, playlistId).then(playlist => {
            console.log(playlist);
        });
    }

    /**
     *
     */
    removeTrackFromPlaylist() {
        console.log(`Remove track ${this.track.name} from ${this.playlist.name}`);
        // this.bluzuService.removeTrackToPlaylist(this.track.id, this.playlist.id).then(playlist => {
            // console.log(playlist);
        // });
    }

    /**
     *
     */
    addToPlayQueue(): void {
        if (this.track !== undefined) {
            this.playerService.addOneToPlayQueue(this.track);
        }
        else if (this.playlist !== undefined) {
            // We need to get tracks from playlist
            // this.playerService.addToPlayQueue(this.playlist.tracks);
            this.bluzuService.getPlaylistTracks(this.playlist.id)
                .subscribe(tracks => this.playerService.addToPlayQueue(tracks));
        }
        else if (this.user !== undefined) {
            // We need to get user tracks
            // this.playerService.addToPlayQueue(user-tracks);
        }
    }

    /**
     *
     */
     delete() {
         if (!this.confirmedDeletion) {
             this.confirmedDeletion = true;
             this.closeMenu = false;
             this.deleteColor = "#ff2e2e";
             this.deleteText = "Click again to confirm";
             this.display = "block";
         }
         else {
             if (this.track !== undefined) {
                 let playingTrack = this.playerService.getPlayingTrack();
                 if (playingTrack && this.track.id === playingTrack.id)
                    this.playerService.playNext();
                 this.bluzuService.deleteTrack(this.track.id);
             }
             else if (this.playlist !== undefined) {
                 this.bluzuService.deletePlaylist(this.playlist.id);
             }

             this.confirmedDeletion = false;
             this.closeMenu = true;
         }
     }

}
