import { Component } from '@angular/core';

@Component({
    moduleId: module.id,
    selector: 'edit-playlist-form',
    templateUrl: 'edit-playlist-form.component.html',
    styleUrls: ['edit-playlist-form.component.css']
})

export class EditPlaylistFormComponent {

}
