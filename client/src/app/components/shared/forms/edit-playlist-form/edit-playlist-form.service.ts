import { Injectable, Output, EventEmitter  } from '@angular/core';

declare let window: any;

@Injectable()
export class EditPlaylistFormService {

    @Output() cancel$: EventEmitter<any> = new EventEmitter();
    @Output() create$: EventEmitter<any> = new EventEmitter();

    cancel(): void {
        console.log("{cancel}");
        this.cancel$.emit(true);
    }

    create(): void {
        console.log("{create}");
        this.create$.emit(true);
    }

}
