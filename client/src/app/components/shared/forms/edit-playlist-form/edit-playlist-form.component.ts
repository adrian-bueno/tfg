import { Component, OnInit, OnChanges, Input, SimpleChanges } from '@angular/core';

import { Playlist } from '../../../../models';
import { BluzuService } from '../../../../services';
import { EditPlaylistFormService } from './edit-playlist-form.service';

declare var ColorThief: any;

interface FileReaderEventTarget extends EventTarget {
    result: string;
    files: any[];
}

interface FileReaderEvent extends Event {
    target: FileReaderEventTarget;
    getMessage(): string;
}

@Component({
    moduleId: module.id,
    selector: 'playlist-form',
    templateUrl: 'edit-playlist-form.component.html',
    styleUrls: ['edit-playlist-form.component.css']
})

export class EditPlaylistFormComponent implements OnInit {

    // @Input() action;

    imageUrl: string = "";
    backgroundColor: string = "#2e8eff";
    textColor: string = "#eee";

    // Form
    name: string = "";
    description: string = "";

    constructor(private bluzuService: BluzuService,
        private editPlaylistFormService: EditPlaylistFormService) { }

    ngOnInit() {
        $(document).ready(function() {
            $("input#name, textarea#description").characterCounter();
        });

        this.editPlaylistFormService.cancel$.subscribe(() => this.cancel());
        this.editPlaylistFormService.create$.subscribe(() => this.create());
    }

    cancel(): void {

        console.log("[[cancel]]");

        // clean modal
        this.imageUrl = "";
        this.name = "";
        this.description = "";
        this.backgroundColor = "#2e8eff";
        this.textColor = "#eee";
    }

    create(): void {

        console.log("[[create]]");

        let metadata: any = {
            name: this.name,
            description: this.description
        };

        this.bluzuService.newPlaylist(metadata).then(playlist => {
            // TODO: check for errors

            Materialize.toast("Playlist created.", 2000, "blue-bg rounded");

            // upload image
            this.bluzuService.uploadPlaylistImage(playlist.id, (<HTMLInputElement>document.getElementById("playlist-image")).files)
                .then(playlist => {
                    Materialize.toast("Playlist image uploaded.", 2000, "blue-bg rounded");
                });

            // clean modal
            this.imageUrl = "";
            this.name = "";
            this.description = "";
            this.backgroundColor = "#2e8eff";
            this.textColor = "#eee";
        });
    }

    /**
     *
     */
    chooseImage() {
        document.getElementById("playlist-image").click();
    }

    /**
    *
    */
    readUrl(event: FileReaderEvent) {
        var reader = new FileReader();

        reader.onload = (e: FileReaderEvent) => {
            this.imageUrl = "url(" + e.target.result + ")";
            this.getColors(e.target.result);
        };

        if (event.target.files[0]) {
            if (event.target.files[0].size > 2048 * 1000) // Max. 2 MB
                Materialize.toast("Max. image size is 2 MB", 4000, "red-bg rounded");
            else {
                reader.readAsDataURL(event.target.files[0]);
                // this.uploadImage();
            }
        }
    }

    /**
    *
    */
    getColors(url: any) {
        let img: any = document.createElement('img');
        img.setAttribute('src', url);

        img.onload = () => {

            let colorThief = new ColorThief();
            let rgb = colorThief.getColor(img);
            // colorThief.getPalette(sourceImage, 8); // 8 color palette
            let dominantColor = `rgb(${rgb[0]}, ${rgb[1]}, ${rgb[2]})`;

            this.backgroundColor = dominantColor;
            this.textColor = this.getNormalizedTextColor(dominantColor);
        };
    }

    /**
    * Helper function to calculate yiq - http://en.wikipedia.org/wiki/YIQ
    * https://github.com/briangonzalez/jquery.adaptive-backgrounds.js
    */
    getYIQ(colorRGB: string): number {
        var rgb: any = colorRGB.match(/\d+/g);
        return ((rgb[0] * 299) + (rgb[1] * 587) + (rgb[2] * 114)) / 1000;
    }

    /**
    * https://github.com/briangonzalez/jquery.adaptive-backgrounds.js
    */
    getNormalizedTextColor(colorRGB: string): string {
        if (this.getYIQ(colorRGB) >= 128)
            return "#111"; // dark
        else
            return "#eee"; // light
    }

}
