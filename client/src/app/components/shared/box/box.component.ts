import { Component, Input, OnInit, AfterViewInit } from '@angular/core';

import { GlobalService, PlayerService, BluzuService } from '../../../services';

@Component({
    moduleId: module.id,
    selector: 'box',
    templateUrl: 'box.component.html',
    styleUrls: ['box.component.css']
})

export class BoxComponent implements OnInit, AfterViewInit {

    @Input() object: any;
    objectRouterLink: string = "";
    displayMoreOptions: boolean = false;
    backgroundColor: string = "";
    textColor: string = "";
    boxWidth: string = "";
    isVisible: boolean = false;
    backgroundImage: string = "";

    constructor(private globalService: GlobalService,
                private playerService: PlayerService,
                private bluzuService: BluzuService) {}

    ngOnInit(): void {
        this.globalService.contentWidth$.subscribe(width => {
            this.onContentWidth(width);
        });

        if (this.object.type === "playlist") {
            this.objectRouterLink = `/playlist/${this.object.id}`;
            this.backgroundColor = this.object.image.dominantColor;
            this.textColor = this.object.image.textColor;
            if (this.object.image.qualities)
                this.backgroundImage = `url(${this.object.image.url}/original.${this.object.image.format})`
            else
                this.backgroundImage = `url(${this.object.image.url})`;
        }
        else if (this.object.type === "user") {
            this.objectRouterLink = `/user/${this.object.id}`;
            this.backgroundImage = `url(${this.object.profileImage})`
        }
        else if (this.object.type === "track") {
            this.objectRouterLink = `/track/${this.object.id}`;
            this.backgroundColor = this.object.image.dominantColor;
            this.textColor = this.object.image.textColor;
            if (this.object.image.qualities)
                this.backgroundImage = `url(${this.object.image.url}/original.${this.object.image.format})`;
            else
                this.backgroundImage = `url(${this.object.image.url})`;
        }
        else {
            this.objectRouterLink = "";
        }
    }

    ngAfterViewInit() {
        // let boxElement = document.getElementById(`${this.object.id}`);
        // this.isVisible = boxElement.isVisible(boxElement);
    }

    // Para probar el modulo in-viewport
    // setItemClass(e: Event) {
    //     // console.log(e);
    //     this.isVisible = e.value;
    // }

    onContentWidth(width: number): void {
        if (width > 2000) {
            this.boxWidth = "275px";
            // this.textColor = "white";
        }
        else if (width > 1800) {
            this.boxWidth = `${width/9 - 5}px`;
            // this.textColor = "purple";
        }
        else if (width > 1600) {
            this.boxWidth = `${width/8 - 5}px`;
            // this.textColor = "red";
        }
        else if (width > 1400) {
            this.boxWidth = `${width/7 - 5}px`;
            // this.textColor = "blue";
        }
        else if (width > 1200) {
            this.boxWidth = `${width/6 - 5}px`;
            // this.textColor = "green";
        }
        else if (width > 1000) {
            this.boxWidth = `${width/5 - 6}px`;
            // this.textColor = "magenta";
        }
        else if (width > 765) {
            this.boxWidth = `${width/4 - 6.25}px`;
            // this.textColor = "cyan";
        }
        else if (width > 515) {
            this.boxWidth = `${width/3 - 6.67}px`;
            // this.textColor = "pink";
        }
        else if (width < 250){ // width < 515
            this.boxWidth = `${width - 10}px`;
            // this.textColor = "yellow";
        }
        else { // 250 < width < 515
            this.boxWidth = `${width/2 - 7.5}px`;
            // this.textColor = "white";
        }
    }

    showOptions() {
        if (this.displayMoreOptions === false)
            this.displayMoreOptions = true;
        else
            this.displayMoreOptions = false;
    }

    play(object: any) {
        if (object.type == "track")
            this.playerService.playTrackPlaylist(object, undefined);
        else if (object.type == "playlist") {
            this.bluzuService.getPlaylistTracks(object.id).subscribe(tracks =>
                this.playerService.playTrackPlaylist(undefined, tracks));
        }
    }

    /**
     *
     */
    contextMenu(event: MouseEvent) {
        event.preventDefault();

        let user;
        let track;
        let playlist;

        if (this.object.type === "user")
            user = this.object;
        if (this.object.type === "track")
            track = this.object;
        if (this.object.type === "playlist")
            playlist = this.object;

        this.globalService.contextMenu({
            top: event.pageY,
            left: event.pageX,
            user: user,
            track: track,
            playlist: playlist
        });
    }

}
