import { Component, OnInit } from '@angular/core';

import { BluzuService, PlayerService } from '../../services';

import { Track, User, Playlist } from '../../models';

@Component({
    moduleId: module.id,
    selector: 'play-queue',
    templateUrl: 'play-queue.component.html',
    styleUrls: ['play-queue.component.css']
})

export class PlayQueueComponent implements OnInit {

    playQueue: Track[] = [];
    playingTrack: Track;

    constructor(private bluzuService: BluzuService, private playerService: PlayerService) {
        playerService.newPlayingTrack$.subscribe(track => this.onNewPlayingTrack(track));
        playerService.newPlayQueue$.subscribe(queue => this.onNewPlayQueue(queue));
    }

    ngOnInit() {
        this.playQueue = this.playerService.getPlayQueue();
        this.playingTrack = this.playerService.getPlayingTrack();
    }

    /**
    *
    */
    onNewPlayingTrack(track: Track): void {
        this.playingTrack = track;
    }

    /**
    *
    */
    onNewPlayQueue(queue: Track[]): void {
        this.playQueue = queue;
        // this.playQueue = this.playerService.getPlayQueue();
    }

    /**
    * Used in template to change style of playing track.
    */
    checkPlayingTrack(track: Track): boolean {
        if (this.playingTrack !== undefined && this.playingTrack.id === track.id)
            return true;
        return false;
    }

    /**
    *
    */
    playThisTrack(track: Track): void {
        this.playerService.playTrackId(track.id);
    }
}
