import { Component, OnInit } from '@angular/core';
import { ipcRenderer } from 'electron';

import { BluzuService, PlayerService } from '../../../services';
import { Track } from '../../../models';

declare var $:any;

@Component({
    moduleId: module.id,
    selector: 'player-big',
    templateUrl: 'player-big.component.html',
    styleUrls: ['player-big.component.css']
})


export class PlayerBigComponent implements OnInit {

    duration: number = 0; //seconds
    currentTime: number = 0; //seconds
    tooltipTime: number = 0; //seconds
    audioPaused: boolean = true;
    percentage: number = 0; // percentage between 0 and 1
    repeatState: string = "off";
    playlistAddVisibility: boolean = false;
    tooltipVolume: boolean = false;
    currentVolume: number = 1.0;
    playingTrack: Track;
    playingTrackAlbumId: string = "";

    constructor(private playerService: PlayerService) {
        playerService.newPlayingTrack$.subscribe(track => this.onNewPlayingTrack(track));
        playerService.ended$.subscribe(() => this.onEndedPlaying());
    }

    /**
    * Funcion called when received "newPlayingTrack" event.
    */
    onNewPlayingTrack(track: Track): void {
        this.playingTrack = track;
        this.duration = track.duration;
        this.playingTrackAlbumId = this.playerService.getPlayingTrackAlbumId();
        this.audioPaused = false;
    }

    /**
    * Funcion called when received "ended" event.
    */
    onEndedPlaying() {
        this.currentTime = 0;
        this.percentage = 0;
        this.audioPaused = true;
    }

    /**
    * Initializes a 0.5 second timer that increases current time indicators when
    * audio is being played.
    * Initializes keyboard events to control player. See keyboardKeyUp() and
    * keyboardKeyDown() for more info.
    */
    ngOnInit() {

        this.playingTrack = this.playerService.getPlayingTrack();
        this.currentTime = this.playerService.getCurrentTime();
        this.duration = this.playerService.getDuration();
        this.percentage = this.currentTime / this.duration;
        this.repeatState = this.playerService.getRepeatState();
        this.playingTrackAlbumId = this.playerService.getPlayingTrackAlbumId();
        this.audioPaused = this.playerService.isPaused();

        // Update currentTime when audio is playing every 0.5 second
        setInterval(() => {
            if (this.playerService.isPaused() === false) { // playing
                this.currentTime = this.playerService.getCurrentTime();
                this.percentage = this.currentTime / this.duration;
            }
        }, 500);
    }

    /**
    * Changes the audio current time and timeline played time bar when the
    * player timeline is clicked this function set the correct audio
    * current time.
    */
    timelineClick(event: MouseEvent) {
        this.percentage = (event.pageX - $("#timeline").position().left) / $("#timeline").width();
        this.currentTime = this.percentage * this.playerService.getDuration();
        this.playerService.setCurrentTime(this.currentTime);
    }

    /**
    *
    */
    timelineMouseMove(event : MouseEvent) {
        let percentage = (event.pageX - $("#timeline").position().left) / $("#timeline").width();
        this.tooltipTime = percentage * this.playerService.getDuration();;
        $("#tooltipTime").css("margin-left", (percentage * 100 - 2.5) + "%"); // Minus 2.5% to center it with the mouse
    }

    /**
    *
    */
    toHHMMSS(seconds: number): string {
        let hh:any = Math.floor(seconds / 3600);
        let mm:any = Math.floor((seconds - (hh * 3600)) / 60);
        let ss:any = Math.floor(seconds - (hh * 3600) - (mm * 60));

        if (hh < 10) { hh = "0" + hh; }
        if (mm < 10) { mm = "0" + mm; }
        if (ss < 10) { ss = "0" + ss; }

        if (hh == 0) {
            return `${mm}:${ss}`;
        }
        else
            return `${hh}:${mm}:${ss}`;
    }

   /**
    * TODO
    */
    volumeClick(event: any) {
        // let percentage: number = (event.pageY - $("#volume-level-container").position().top) / $("#volume-level-container").height();
        // this.currentVolume = percentage * this.playerService.getDuration();;
        this.currentVolume = (event.pageY + $("#volume-level-container").position().top) / $("#volume-level-container").height();
        // $("#volume-level").height(this.currentVolume * 100 + "%");
        // Globals.audio.volume = this.currentVolume;

        // console.log("pageY = " + event.pageY);
        // console.log("top = " + $("#volume-level-container").position().top);
        // console.log("height = " + $("#volume-level-container").height());
        // console.log("currentVolume = " + this.currentVolume);
        // console.log("audio.volume = " + Globals.audio.volume);
        // console.log("---------------------------------------------------");
    }

   /**
    * Plays audio if it is paused, pause audio if it is playing.
    */
    playPause() {
        if (this.playerService.isPaused() === true) {
            this.playerService.play();
            this.audioPaused = false;
        }
        else {
            this.playerService.pause();
            this.audioPaused = true;
        }
    }

   /**
    * Plays the next track in play queue.
    */
    next() {
        this.playerService.playNext();
    }

   /**
    * Plays the previous track in play queue.
    */
    previous() {
        this.playerService.playPrevious();
    }

   /**
    * TODO
    * Favorite playing track.
    */
    favorite() {
        if($("#favorite").html() === "favorite")
            $("#favorite").html("favorite_border");
        else
            $("#favorite").html("favorite");

        $("#favorite").toggleClass("button-active");
    }

   /**
    * TODO
    * Repost playing track.
    */
    repost() {
        $("#repost").toggleClass("button-active");
    }

   /**
    * TODO
    * Add playing track to a playlist.
    */
    addToPlaylist() {
        if (this.playlistAddVisibility === false) {
            $("#tooltipPlaylist").css("visibility", "visible");
            $("#tooltipPlaylist").css("opacity", "1");
            $("#addToPlaylist").toggleClass("button-active");
            this.playlistAddVisibility = true;
        }
        else {
            $("#tooltipPlaylist").css("visibility", "hidden");
            $("#tooltipPlaylist").css("opacity", "0");
            $("#addToPlaylist").toggleClass("button-active");
            this.playlistAddVisibility = false;
        }
    }

   /**
    * TODO
    * Download playing track.
    */
    download() {

    }

   /**
    * TODO
    * Shares playing track.
    */
    share() {

    }

   /**
    * Changes repeat state. If repeat off, audio is paused when playing track
    * ends and there are no more tracks in play queue. If repeat all, when the
    * last track in queue ends, the play queue is played again. If repeat one,
    * when playing track ends, it starts playing again from the beginning.
    */
    repeat() {
        // If repeat is off, change to repeat all
        if (this.repeatState === "off") {
            this.repeatState = "all";
            this.playerService.repeatAll();
        }
        // If repeat all is active, change to repeat one
        else if (this.repeatState === "all") {
            this.repeatState = "one";
            this.playerService.repeatOne();
        }
        // If repeat one is active, change to repeat off
        else {
            this.repeatState = "off";
            this.playerService.repeatOff();
        }
    }

   /**
    * TODO
    * Activates shuffle mode, which suffle queue tracks.
    */
    shuffle() {
        $("#shuffle").toggleClass("button-active");
    }

   /**
    * Show or hide the volume level indicator.
    */
    volume() {
        if (this.tooltipVolume === false) {
            $("#tooltipVolume").css("visibility", "visible");
            $("#tooltipVolume").css("opacity", "1");
            $("#volume").toggleClass("button-active");
            this.tooltipVolume = true;
        }
        else {
            $("#tooltipVolume").css("visibility", "hidden");
            $("#tooltipVolume").css("opacity", "0");
            $("#volume").toggleClass("button-active");
            this.tooltipVolume = false;
        }
    }

   /**
    * Turn up 5% of audio volume.
    */
    volumeUp() {
        // if (Globals.audio.volume < 1) {
        //     Globals.audio.volume += 0.05;
        //     if (Globals.audio.volume > 0.95){
        //         Globals.audio.volume = 1;
        //     }
        //     $("#volume-level").height(Globals.audio.volume * 100 + "%");
        // }
    }

   /**
    * Turn down 5% of audio volume.
    */
    volumeDown() {
        // if (Globals.audio.volume > 0.001) {
        //     Globals.audio.volume -= 0.05;
        //     if (Globals.audio.volume < 0.05){
        //         Globals.audio.volume = 0;
        //     }
        //     $("#volume-level").height(Globals.audio.volume * 100 + "%");
        // }
    }

}
