import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';

import { BluzuService } from '../../../services';
import { Track } from '../../../models';

@Component({
    moduleId: module.id,
    selector: 'user-music',
    templateUrl: 'user-music.component.html',
    styleUrls: ['user-music.component.css']
})

export class UserMusicComponent implements OnInit {

    userId: string;
    tracks: Track[];

    constructor(
        private bluzuService: BluzuService,
        private route: ActivatedRoute,
        private router: Router
    ) { }

    ngOnInit(): void {
        this.route.parent.params.forEach((params: Params) => {
            this.userId = params["id"];

            this.bluzuService.getUserTracks(this.userId).subscribe(tracks => {
                this.tracks = tracks;
            });
        });
    }
}
