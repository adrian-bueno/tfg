import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';

import { BluzuService } from '../../../services';
import { Playlist } from '../../../models';

@Component({
    moduleId: module.id,
    selector: 'user-playlists',
    templateUrl: 'user-playlists.component.html',
    styleUrls: ['user-playlists.component.css']
})

export class UserPlaylistsComponent implements OnInit {

    userId: string;
    playlists: Playlist[];

    constructor(
        private bluzuService: BluzuService,
        private route: ActivatedRoute,
        private router: Router
    ) { }

    ngOnInit(): void {
        this.route.parent.params.forEach((params: Params) => {
            this.userId = params["id"];

            this.bluzuService.getUserPlaylists(this.userId).subscribe(playlists => {
                this.playlists = playlists;
            });
        });
    }
}
