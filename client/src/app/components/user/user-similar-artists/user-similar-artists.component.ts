import { Component } from '@angular/core';

@Component({
    moduleId: module.id,
    selector: 'user-similar-artists',
    templateUrl: 'user-similar-artists.component.html',
    styleUrls: ['user-similar-artists.component.css']
})

export class UserSimilarArtistsComponent {

}
