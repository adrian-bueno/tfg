import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';

import { User, Track } from '../../models';
import { BluzuService, PlayerService } from '../../services';

@Component({
    moduleId: module.id,
    selector: 'user',
    templateUrl: 'user.component.html',
    styleUrls: ["user-header.css", "user-navbar.css"]
})

export class UserComponent implements OnInit {

    user: User;
    profileImage: string = "";
    backgroundImage: string = "";
    playingTrack: Track;

    /**
    *
    */
    constructor(
        private bluzuService: BluzuService,
        private playerService: PlayerService,
        private route: ActivatedRoute,
    ) {
        playerService.newPlayingTrack$.subscribe(track => this.onNewPlayingTrack(track));
    }

    /**
    *
    */
    onNewPlayingTrack(track: Track): void {
        this.playingTrack = track;
    }

    /**
    *
    */
    ngOnInit() {
        this.route.params.forEach((params: Params) => {
            let id: string = params['id'];
            this.bluzuService.getUser(id)
                .then(user => {
                    this.user = user;

                    if (user.profileImage)
                        this.profileImage = `url(${user.profileImage})`;
                    if (user.backgroundImage)
                        this.backgroundImage = `url(${user.backgroundImage})`;
                });
        });

        // Get playing track
        this.playingTrack = this.playerService.getPlayingTrack();
    }

    play() {
        console.log("user.component");
        console.log(this.user);
    }
}
