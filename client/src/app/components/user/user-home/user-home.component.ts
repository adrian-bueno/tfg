import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';

import { BluzuService } from '../../../services';
import { Track, Playlist } from '../../../models';

@Component({
    moduleId: module.id,
    selector: 'user-home',
    templateUrl: 'user-home.component.html',
    styleUrls: ['user-home.component.css']
})

export class UserHomeComponent implements OnInit, OnDestroy {

    // private sub: any;
    userId: string;
    playlists: Playlist[] = [];
    tracks: Track[] = [];

    constructor(
        private bluzuService: BluzuService,
        private route: ActivatedRoute,
        private router: Router
    ) { }

    ngOnInit(): void {
        this.route.parent.params.forEach((params: Params) => {
            this.userId = params["id"];

            this.bluzuService.getUserPlaylists(this.userId).subscribe(playlists => {
                this.playlists = playlists ? playlists : [];
            });

            this.bluzuService.getUserTracks(this.userId).subscribe(tracks => {
                this.tracks = tracks ? tracks : [];
            });
        });

        // this.sub = this.route.parent.params.subscribe(params => {
        //     this.userId = params["id"];
        //     console.log(this.userId);
        // });
    }

    ngOnDestroy(): void {
        // this.sub.unsubscribe();
    }

}
