import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';

import { User, Playlist } from "../../models";
import { BluzuService, GlobalService } from "../../services";

declare var $: any;

@Component({
    moduleId: module.id,
    selector: 'sidenav',
    templateUrl: 'sidenav.component.html',
    styleUrls: ['sidenav.component.css']
})

export class SidenavComponent implements OnInit {

    loggedUser: User;
    mePlaylists: Playlist[] = [];
    otherPlaylists: Playlist[] = [];

    constructor(
        private bluzuService: BluzuService,
        private globalService: GlobalService,
        private location: Location) {
    }

    ngOnInit() {
        this.loggedUser = this.bluzuService.getLoggedUser();

        this.bluzuService.logEmitter$.subscribe(user => {
            this.loggedUser = user;
        });

        this.bluzuService.newPlaylist$.subscribe(playlist => {
            this.mePlaylists.push(playlist);
        });

        this.bluzuService.deletedPlaylist$.subscribe(playlistId => {
            this.onDeletedPlaylist(playlistId);
        });

        this.bluzuService.getMePlaylists()
            .subscribe(playlists => this.mePlaylists = playlists);

        $(document).ready(function() {
            // the "href" attribute of .modal-trigger must specify the modal ID that wants to be triggered
            $('.modal').modal();
        });
    }

    /**
     *
     */
    onDeletedPlaylist(playlistId: string): void {
        let i = 0;
        for (let playlist of this.mePlaylists) {
            if (playlist.id === playlistId) {
                this.mePlaylists.splice(i, 1);
                break;
            }
            i++;
        }
    }

    userAccountShowMore() {
        $("#user-account-more").toggleClass("display-none");
    }

    logout(): void {
        this.bluzuService.logout();
        // window.location.reload();
    }

    goPreviousPage(): void {
        this.location.back();
        // window.history.back();
    }

    goForwardPage(): void {
        this.location.forward();
        // window.history.forward();
    }

    contextMenu(event: MouseEvent, playlist: Playlist) {
        event.preventDefault();

        if (playlist !== undefined) {
            this.globalService.contextMenu({
                top: event.pageY,
                left: event.pageX,
                user: undefined,
                track: undefined,
                playlist: playlist
            });
        }
    }

}
