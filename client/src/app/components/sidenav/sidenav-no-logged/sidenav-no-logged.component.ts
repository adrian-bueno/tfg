import { Component } from '@angular/core';
import { Location } from '@angular/common';
// Comentado porque no funciona con ngc aot. Descomentar para utilizar con electron.
// import { ipcRenderer } from 'electron';

import { BluzuService } from '../../../services';

@Component({
    moduleId: module.id,
    selector: 'sidenav-no-logged',
    templateUrl: 'sidenav-no-logged.component.html',
    styleUrls: ['sidenav-no-logged.component.css']
})

export class SidenavNoLoggedComponent {

    constructor(private bluzuService: BluzuService, private location: Location) {}

    login(action: string) {
        let url: string = this.bluzuService.getOauth2Url();

        if (action === "register")
            url += "#register";

        // Comentado porque no funciona con ngc aot. Descomentar para utilizar con electron.
        // if (typeof process === 'object' && process + '' === '[object process]') {
        //     ipcRenderer.send('login', url);
        //     ipcRenderer.on('logged', (event: any, code: any) => {
        //         this.bluzuService.login(code);
        //         ipcRenderer.removeAllListeners("logged")
        //     })
        // }
        // else {
        //     window.location.href = url;
        // }
        window.location.href = url;
    }

    goPreviousPage(): void {
        this.location.back();
        // window.history.back();
    }

    goForwardPage(): void {
        this.location.forward();
        // window.history.forward();
    }

    // /**
    //  * Opens a window popup centered.
    //  * http://stackoverflow.com/questions/4068373/center-a-popup-window-on-screen
    //  */
    // popupCenter(url, title, w, h) {
    //     // Fixes dual-screen position                         Most browsers      Firefox
    //     // let dualScreenLeft = window.screenLeft != undefined ? window.screenLeft : screen.left;
    //     // let dualScreenTop = window.screenTop != undefined ? window.screenTop : screen.top;
    //     let dualScreenLeft = window.screenLeft;
    //     let dualScreenTop = window.screenTop;
    //
    //     let width = window.innerWidth ? window.innerWidth : document.documentElement.clientWidth ? document.documentElement.clientWidth : screen.width;
    //     let height = window.innerHeight ? window.innerHeight : document.documentElement.clientHeight ? document.documentElement.clientHeight : screen.height;
    //
    //     let left = ((width / 2) - (w / 2)) + dualScreenLeft;
    //     let top = ((height / 2) - (h / 2)) + dualScreenTop;
    //     let newWindow = window.open(url, title, 'scrollbars=yes, width=' + w + ', height=' + h + ', top=' + top + ', left=' + left);
    //
    //     // Puts focus on the newWindow
    //     if (window.focus) {
    //         newWindow.focus();
    //     }
    // }
}
