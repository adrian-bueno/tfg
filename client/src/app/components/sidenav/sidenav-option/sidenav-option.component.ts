import { Component, Input } from '@angular/core';

import { Playlist } from '../../../models';

@Component({
    moduleId: module.id,
    selector: 'sidenav-option',
    templateUrl: 'sidenav-option.component.html',
    styleUrls: ['sidenav-option.component.css']
})

export class SidenavOptionComponent {
    @Input() playlist: Playlist;
}
