import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';

import { Playlist, User, Track } from '../../models';
import { BluzuService } from '../../services';

@Component({
    moduleId: module.id,
    selector: 'home',
    templateUrl: 'home.component.html',
    styleUrls: ['home.component.css']
})

export class HomeComponent implements OnInit {

    playlists: Playlist[];
    users: User[];
    tracks: Track[];

    /**
    *
    */
    constructor(
        private bluzuService: BluzuService,
        private route: ActivatedRoute,
        private router: Router
    ) { }

    /**
     *
     */
    ngOnInit() {
        this.route.queryParams.forEach((params: Params) => {
            if (params['code'] && !this.bluzuService.getLoggedUser()) {
                this.bluzuService.login(params['code']);
                this.router.navigate(['']);
            }
        });

        this.bluzuService.deletedTrack$.subscribe(trackId => this.onDeletedTrack(trackId));
        this.bluzuService.deletedPlaylist$.subscribe(playlistId => this.onDeletedPlaylist(playlistId));
        this.bluzuService.newPlaylist$.subscribe(playlist => this.onNewPlaylist(playlist));
        this.bluzuService.newTrack$.subscribe(track => this.tracks.push(track));

        this.getPlaylists();
        this.getUsers();
        this.getTracks();
    }

    /**
     *
     */
    onNewPlaylist(playlist: Playlist): void {
        setTimeout(this.bluzuService.getPlaylist(playlist.id).then(p => {
                this.playlists.push(p);
        }), 2000);
    }

    /**
     *
     */
    onDeletedTrack(trackId: string): void {
        let i = 0;
        for (let track of this.tracks) {
            if (track.id === trackId) {
                this.tracks.splice(i, 1);
                break;
            }
            i++;
        }
    }

    /**
     *
     */
    onDeletedPlaylist(playlistId: string): void {
        let i = 0;
        for (let playlist of this.playlists) {
            if (playlist.id === playlistId) {
                this.playlists.splice(i, 1);
                break;
            }
            i++;
        }
    }

    /**
     *
     */
    getPlaylists(): void {
        //this.bluzuService.getPlaylists().then(playlists => this.playlists = playlists);

        this.bluzuService.getAllPlaylists().subscribe(playlists => {
            this.playlists = playlists;
        });
    }

    /**
     *
     */
    getUsers(): void {
        this.bluzuService.getAllUsers().subscribe(users => {
            this.users = users;
        });
    }

    /**
     *
     */
    getTracks(): void {
        this.bluzuService.getAllTracks().subscribe(tracks => {
            this.tracks = tracks;
        });
    }

}
