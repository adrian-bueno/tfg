import { Component, OnInit } from '@angular/core';

import { Playlist, User, Track } from '../../models';

import { BluzuService } from '../../services';

declare var $: any;

@Component({
    moduleId: module.id,
    selector: 'search',
    templateUrl: 'search.component.html',
    styleUrls: ['search.component.css']
})

export class SearchComponent implements OnInit {

    playlists: Playlist[] = [];
    users: User[] = [];
    tracks: Track[] = [];
    searchInput: string;

    constructor(private bluzuService: BluzuService) { }

    ngOnInit() {

    }

    search(searchString: string) {
        console.log("hola");

        if (searchString === "") {
            this.playlists = [];
            this.users = [];
            this.tracks = [];
            return;
        }

        this.bluzuService.searchPlaylists(searchString)
            .subscribe(playlists => this.playlists = playlists);

        this.bluzuService.searchUsers(searchString)
            .subscribe(users => this.users = users);

        this.bluzuService.searchTracks(searchString)
            .subscribe(tracks => this.tracks = tracks);
    }

}
