import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { Location } from '@angular/common';

import { BluzuService, PlayerService, GlobalService } from '../../services';
import { Playlist, Track, User } from '../../models';

declare var $: any;

@Component({
    moduleId: module.id,
    selector: 'playlist',
    templateUrl: 'playlist.component.html',
    styleUrls: ['playlist.component.css']
})

export class PlaylistComponent implements OnInit {

    playlist: Playlist;
    tracks: Track[] = [];           // Playlist tracks
    releaseDate: any;
    numTracks: number = 0;
    totalTimeSec: number = 0;  // in seconds
    totalTime: string;         // "x hr y min"
    playingTrack: Track;
    playlistImage: string;
    runningOnMobile: boolean = false;

    constructor(
        private bluzuService: BluzuService,
        private playerService: PlayerService,
        private globalService: GlobalService,
        private route: ActivatedRoute,
        private location: Location
    ) {
        if (globalService.runningOnMobile()) {
            this.runningOnMobile = true;
        }
    }

    /**
    *
    */
    onNewPlayingTrack(track: Track): void {
        this.playingTrack = track;
    }

    /**
     *
     */
    onDeletedTrack(trackId: string): void {
        let i = 0;
        for (let track of this.tracks) {
            if (track.id === trackId) {
                this.tracks.splice(i, 1);
                break;
            }
            i++;
        }
    }

    /**
    * Used in template to change style of playing track.
    */
    checkPlayingTrack(track: Track): boolean {
        if (this.playingTrack !== undefined && this.playingTrack.id === track.id)
            return true;
        return false;
    }

    /**
    *
    */
    ngOnInit() {

        this.playerService.newPlayingTrack$.subscribe(track => this.onNewPlayingTrack(track));
        this.bluzuService.deletedTrack$.subscribe(trackId => this.onDeletedTrack(trackId));

        // Get the playlist
        this.route.params.forEach((params: Params) => {
            let id: string = params['id'];

            this.bluzuService.getPlaylist(id)
                .then(playlist => {
                    this.playlist = playlist;

                    if (playlist.image.qualities)
                        this.playlistImage = `url(${playlist.image.url}/original.${playlist.image.format})`
                    else
                        this.playlistImage = `url(${playlist.image.url})`

                    this.releaseDate = new Date(this.playlist.releaseDate).getFullYear();
                });

            this.bluzuService.getPlaylistTracks(id).subscribe(tracks => {
                this.tracks = tracks;
                this.numTracks = this.tracks.length;

                for (let track of this.tracks) {
                    this.totalTimeSec += track.duration;
                }

                this.totalTime = this.toHHMMSS(this.totalTimeSec);
            });
        });

        // Get playing track
        this.playingTrack = this.playerService.getPlayingTrack();
    }

    /**
    *
    */
    toHHMMSS(seconds: number): string {
        let hh:any = Math.floor(seconds / 3600);
        let mm:any = Math.floor((seconds - (hh * 3600)) / 60);
        let ss:any = Math.floor(seconds - (hh * 3600) - (mm * 60));

        if (hh < 10) { hh = "0" + hh; }
        if (mm < 10) { mm = "0" + mm; }
        if (ss < 10) { ss = "0" + ss; }

        if (hh == 0) {
            return `${mm} min`; //  ${ss} seg
        }
        else
            return `${hh} hr ${mm} min`; //  ${ss} seg
    }

    addOneToPlayQueue(track: Track) {
        if (track)
            this.playerService.addOneToPlayQueue(track);
    }

    addToPlayQueueAndListen(listOfTracks: Track[]) {
        if (listOfTracks.length > 0) {
            this.playerService.cleanPlayQueue();
            this.playerService.addToPlayQueueAndPlay(listOfTracks);
        }
    }

    /**
    * TODO
    * Show a play button at the track row
    */
    // NO FUNCIONA CORRECTAMENTE
    showPlayButton(trackId: string) {
        // Need to add style here because it does not get the style from the css.
        // $(`#${trackId} .track-number`).html("<i \
        //     style='width:10px;margin-left:2px;margin-top:5px;font-size:20px;z-index:-1' \
        //     class='material-icons'>play_circle_filled</i>");
        // $(`#${trackId} .track-number div`).html("<i \
        //     style='width:10px;margin-left:2px;margin-top:5px;font-size:20px;z-index:-1' \
        //     class='material-icons'>play_circle_filled</i>");

        // $(`#${trackId} div`).toggleClass("hover-hide");
        // $(`#${trackId} .play-button`).toggleClass("hover-hide");
    }

    /**
    * TODO
    * Hide the play button at the track row
    */
    // NO FUNCIONA CORRECTAMENTE
    hidePlayButton(trackId: string, trackNumber: number) {
        // $(`#${trackId} .track-number`).html(trackNumber);

        // $(`#${trackId} .track-number div`).html(trackNumber);

        // $(`#${trackId} div`).toggleClass("hover-hide");
        // $(`#${trackId} .play-button`).toggleClass("hover-hide");
    }

    /**
    * TODO
    */
    favouriteThisTrack(trackId: string) {
        if ($(`#${trackId} .like-button .material-icons`).html() == "favorite")
            $(`#${trackId} .like-button .material-icons`).html("favorite_border");
        else
            $(`#${trackId} .like-button .material-icons`).html("favorite");
    }

    /**
    *
    */
    playThisTrack(track: Track) {
        // this.playerService.playTrackPlaylist(track, this.playlist);
        this.playerService.playTrackPlaylist(track, this.tracks);
    }

    /**
     *
     */
    contextMenu(event: any, track: Track) {
        event.preventDefault();

        this.globalService.contextMenu({
            top: event.pageY,
            left: event.pageX,
            user: undefined,
            track: track,
            playlist: this.playlist
        });
    }

}
