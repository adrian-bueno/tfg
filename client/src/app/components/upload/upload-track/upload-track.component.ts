import { Component, OnInit, Input } from '@angular/core';

import { Playlist, Track, User } from '../../../models';
import { BluzuService } from '../../../services';

declare var ColorThief: any;

interface FileReaderEventTarget extends EventTarget {
    result: string;
    files: any[];
}

interface FileReaderEvent extends Event {
    target: FileReaderEventTarget;
    getMessage(): string;
}

@Component({
    moduleId: module.id,
    selector: 'upload-track',
    templateUrl: 'upload-track.component.html',
    styleUrls: ['upload-track.component.css']
})

export class UploadTrackComponent implements OnInit {

    @Input() track: Track;
    @Input() userPlaylists: Playlist[];

    imageUrl: string = "";
    backgroundColor: string = "#2e8eff";
    textColor: string = "#eee";
    imageHtmlId: string = "";
    audioHtmlId: string = "";
    name: string;
    artistsN: string;
    genres: string;

    constructor(private bluzuService: BluzuService) { }

    /**
    *
    */
    ngOnInit() {

        // Initialize Materialize components
        $('.datepicker').pickadate({
            selectMonths: true, // Creates a dropdown to control month
            selectYears: 100 // Creates a dropdown of 15 years to control year
        });

        $(document).ready(function() {
            $('select').material_select();
        });

        //
        this.imageHtmlId = `image${this.track.id}`;
        this.audioHtmlId = `audio${this.track.id}`;
    }

    explicit(track: Track) {
        if (track.explicit === undefined || track.explicit === false) {
            track.explicit = true;
            document.getElementById(`checkbox${track.id}`).setAttribute("checked", "checked");
            // $()
        }
        else {
            track.explicit = false;
            document.getElementById(`checkbox${track.id}`).removeAttribute("checked");
        }
    }

    /**
     *
     */
    saveMetaData() {

        let newData: any = {};

        if (this.name !== undefined && this.name !== "")
            newData.name = this.name;

        if (this.artistsN !== undefined && this.artistsN !== "") {
            newData.artistsN = this.artistsN.split(",");
            for (let i = 0; i < newData.artistsN.length; i++)
                newData.artistsN[i] = newData.artistsN[i].trim();
        }

        if (this.genres) {
            newData.genres = this.genres.split(",");
            for (let i = 0; i < newData.genres.length; i++)
                newData.genres[i] = newData.genres[i].trim();
        }

        newData.explicit = this.track.explicit;

        this.bluzuService.updateTrack(this.track.id, newData).then(track => {
            console.log(track);
            this.track = track;
        });

        // Add to playlists
        console.log($("#playlistsToAdd").val());

        for (let playlistId of $("#playlistsToAdd").val()) {
            this.bluzuService.addTrackToPlaylist(this.track.id, playlistId).then(playlist => console.log(playlist));
        }

    }

    /**
     *
     */
    uploadAudio() {
        console.log("uploading audio ...");
        this.bluzuService.uploadTrackAudio(this.track.id, (<HTMLInputElement>document.getElementById(this.audioHtmlId)).files)
            .then(track => {
                this.track = track;
                console.log("audio uploaded!");
            });
    }

    /**
     *
     */
    chooseImage() {
        document.getElementById(this.imageHtmlId).click();
    }

    /**
     *
     */
    uploadImage() {
        this.bluzuService.uploadTrackImage(this.track.id, (<HTMLInputElement>document.getElementById(this.imageHtmlId)).files)
            .then(track => {
                this.track = track;
            });
    }

    /**
    *
    */
    readUrl(event: FileReaderEvent) {
        var reader = new FileReader();

        reader.onload = (e: FileReaderEvent) => {
            this.imageUrl = "url(" + e.target.result + ")";
            this.getColors(e.target.result);
        };

        if (event.target.files[0]) {
            if (event.target.files[0].size > 2048 * 1000) // Max. 2 MB
                Materialize.toast("Max. image size is 2 MB", 4000, "red-bg rounded");
            else {
                reader.readAsDataURL(event.target.files[0]);
                this.uploadImage();
            }
        }
    }

    /**
    *
    */
    getColors(url: any) {
        let img: any = document.createElement('img');
        img.setAttribute('src', url);

        img.onload = () => {

            let colorThief = new ColorThief();
            let rgb = colorThief.getColor(img);
            // colorThief.getPalette(sourceImage, 8); // 8 color palette
            let dominantColor = `rgb(${rgb[0]}, ${rgb[1]}, ${rgb[2]})`;

            this.backgroundColor = dominantColor;
            this.textColor = this.getNormalizedTextColor(dominantColor);
        };
    }

    /**
    * Helper function to calculate yiq - http://en.wikipedia.org/wiki/YIQ
    * https://github.com/briangonzalez/jquery.adaptive-backgrounds.js
    */
    getYIQ(colorRGB: string): number {
        var rgb: any = colorRGB.match(/\d+/g);
        return ((rgb[0] * 299) + (rgb[1] * 587) + (rgb[2] * 114)) / 1000;
    }

    /**
    * https://github.com/briangonzalez/jquery.adaptive-backgrounds.js
    */
    getNormalizedTextColor(colorRGB: string): string {
        if (this.getYIQ(colorRGB) >= 128)
            return "#111"; // dark
        else
            return "#eee"; // light
    }

}
