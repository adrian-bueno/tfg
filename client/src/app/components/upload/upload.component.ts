import { Component, OnInit } from '@angular/core';

import { Playlist, Track, User } from '../../models';
import { BluzuService } from '../../services';

declare var $: any;

@Component({
    moduleId: module.id,
    selector: 'upload',
    templateUrl: 'upload.component.html',
    styleUrls: ['upload.component.css']
})

export class UploadComponent implements OnInit {

    tracks: Track[] = [];
    userPlaylists: Playlist[] = [];

    constructor(private bluzuService: BluzuService) {}

    /**
     *
     */
    ngOnInit() {
        this.bluzuService.getMePlaylists("name").subscribe(playlists => this.userPlaylists = playlists);
    }

    /**
     *
     */
    addOneMoreTrack() {
        $("#add-one-more-track").html("PLEASE WAIT...");

        this.bluzuService.newTrack().then(track => {
            this.tracks.push(track);
            $("#add-one-more-track").html("ADD 1 MORE TRACK");
        });
    }

    /**
     *
     */
    deleteTrack(track: Track) {

        this.bluzuService.deleteTrack(track.id).then(success => {
            if (success) {
                console.log("[[[ track deleted ]]]");

                let index = this.tracks.indexOf(track);
                if (index > -1) {
                    this.tracks.splice(index, 1);
                }
            }
            else
                console.log("[[[ track NOT deleted ]]]");
        });
    }

    /**
     *
     */
    debug() {
        console.log("--- playlists ---");
        console.log(this.userPlaylists);
        console.log("--- tracks ---");
        console.log(this.tracks);
    }


}
