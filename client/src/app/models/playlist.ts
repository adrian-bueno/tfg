import { User } from './user';
import { PlaylistTrack } from './playlist-track';
import { Image } from './image';

export class Playlist {
    createdDate : Date;            // Playlist creation date.
    description : string;          // Description.
    genres      : string[];        // Playlist genres.
    id          : string;          // Playlist id.
    image       : Image;           // Playlist image.
    isAlbum     : boolean;         // If the playlist is an album.
    labels      : string[];        // Labels.
    name        : string;          // Name.
    public      : boolean;         // True if playlist is public, false if is private.
    owner       : User;          // Id of the user who created it.
    releaseDate : Date;            // If is an album, the release date of the album.
    tracks      : PlaylistTrack[]; // List of playlistTracks.
}
