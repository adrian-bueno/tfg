export class Image {
    dominantColor: string;  // The color that appears more in the image.
    textColor: string;      // Recommended text color to use with the dominantColor.
    url: string;            // The source URL of the image.
    qualities: number[];
    format: string;
}
