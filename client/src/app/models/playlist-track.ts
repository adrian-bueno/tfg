import { User } from './user';
import { Track } from './track';

export class PlaylistTrack {
    addedAt : Date;    // UTC time.
    addedBy : string;  // Id of the user who added it to the playlist.
    id      : string;  // Track id.
}
