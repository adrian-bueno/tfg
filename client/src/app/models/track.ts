import { User } from './user';
import { Image } from './image';

export class Track {
    albumId     : string;    // Album ID in which the track appears.
    artists     : string[];  // Artists who performed the track and have an account in bluzu.
    artistsN    : string[]; // Artists who performed the track and DO NOT have an account in bluzu.
    createdDate : Date;     // Date when the track was created in server.
    duration    : number;   // The track length in seconds.
    explicit    : boolean;  // Whether or not the track has explicit lyrics (true = yes it does; false = no it does not OR unknown).
    genres      : string[]; // Genres.
    id          : string;   // The ID for the track.
    isPlayable  : boolean;  // The track might not be playable if the owner did not upload the track audio.
    image       : Image;    // Track image.
    name        : string;   // The name of the track.
    owner       : User;
    // owner       : string;   // Id of the user who created the track.
    public      : boolean;  // True if track is public, false if is private.
    // url         : string;   // A link to the track.
    url         : { html5: string, dash: string }
    trackNumber : number;   // The number of the track.
    type        : string;   // The object type: "track".
}
