import { Image } from './image';

export class User {
    followers: any;
    genres: string;
    id: string;
    username: string;
    // images: Image[];
    profileImage: string;
    backgroundImage: string;
    name: string;
    type: string;
    public: boolean;
    birthdate: string;
    country: string;
    emails: string[];
}
