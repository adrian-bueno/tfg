import { Injectable, Output, EventEmitter  } from '@angular/core';
import { Http, Headers } from '@angular/http';
import { Observable } from 'rxjs';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';

import { Playlist, User, Track } from '../models';

/*
 * TODO:
 * - Catch http errors
 */

@Injectable()
export class BluzuService {

    loggedUser: User;
    accessToken: string;
    bluzuApiBaseUrl: string = "http://127.0.0.1:8080";
    baseUrl: string = "http://127.0.0.1";
    clientId: string = "593871faef2588109750591a";
    clientSecret: string = "po3y4x8cr0434d1e4ylt2fwn0pr07klolsn57dxqpce9lak2q72r354hg667ev94c0n3uj8hhh3tljhb1ahl0jup2q4tu1ya5pm7ff2fjihf9b4c0xma9fapwt9gvxhy";
    redirectUri: string = encodeURI(`${this.baseUrl}/`);
    authorizeUri: string = `${this.bluzuApiBaseUrl}/oauth2/authorize?client_id=${this.clientId}&response_type=code&redirect_uri=${this.redirectUri}`;

    // Emitted when user login/logout
    @Output() logEmitter$: EventEmitter<User> = new EventEmitter();

    @Output() newTrack$: EventEmitter<Track> = new EventEmitter();
    @Output() deletedTrack$: EventEmitter<string> = new EventEmitter();
    @Output() newPlaylist$: EventEmitter<Playlist> = new EventEmitter();
    @Output() deletedPlaylist$: EventEmitter<string> = new EventEmitter();

    /**
     *
     */
    constructor(private http: Http) {
        // Load last playing track and last playQueue
        this.loadBluzuService();

        // Catchs event triggered when window is closed.
        // It saves blzu service data in local storage.
        // These data is loaded again when the app starts.
        // window.onbeforeunload = () => this.saveBluzuService();
    }

/*============================================================================*/
/*  LOCAL STORAGE                                                             */
/*============================================================================*/

    /**
     *
     */
    private loadBluzuService(): void {
        if (typeof (Storage) !== "undefined") {
            try {
                this.loggedUser = JSON.parse(localStorage.getItem("loggedUser"));
                this.accessToken = JSON.parse(localStorage.getItem("accessToken"));

                this.logEmitter$.emit(this.loggedUser);
            }
            catch (err) {
                console.log("No saved data in local storage.");
                console.log(err);

                this.loggedUser = undefined;
                this.accessToken = undefined;
                this.logEmitter$.emit(this.loggedUser);
            }
        }
        else {
            console.log("No Web Storage support..");
        }
    }

    /**
     *
     */
    private saveBluzuService(): void {
        if (typeof (Storage) !== "undefined") {
            try {
                if (this.loggedUser)
                    localStorage.setItem("loggedUser", JSON.stringify(this.loggedUser));
                if (this.accessToken)
                    localStorage.setItem("accessToken", JSON.stringify(this.accessToken));
            }
            catch (err) {
                console.log("Error saving bluzu service data in local storage.");
                console.log(err);
            }
        }
        else {
            console.log("No Web Storage support..");
        }
    }

/*============================================================================*/
/*  AUTH                                                                      */
/*============================================================================*/

    /**
     *
     */
    getLoggedUser(): User {
        return this.loggedUser;
    }

    /**
     *
     */
    getOauth2Url(): string {
        return this.authorizeUri;
    }

    /**
     *
     */
    private getToken(code: string) {
        let authorization: string = "Basic " + btoa(this.clientId + ":" + this.clientSecret);

        let headers: Headers = new Headers();
        headers.append("content-type", "application/x-www-form-urlencoded");
        headers.append("authorization", authorization);

        let body: string = `code=${code}&grant_type=authorization_code&redirect_uri=${this.redirectUri}`;

        let res: any = this.http.post(this.bluzuApiBaseUrl + "/oauth2/token", body, { headers: headers })
            .map(res => res.json());
        // .catch((error:any) => Observable.throw(error.json().error || 'Server error')); //...errors if

        return res;
    }

    /**
     *
     */
    login(code: string) {
        this.getToken(code).subscribe(res => {
            this.accessToken = res.access_token.value; // TODO: comprobar si no se obtiene token
            this.getMe().subscribe(user => {
                this.loggedUser = user;
                this.logEmitter$.emit(this.loggedUser);
                this.saveBluzuService();
            });
        });
    }

    /**
     * TODO (optional): clear player queue too.
     */
    logout() {
        this.loggedUser = undefined;
        this.accessToken = undefined;
        this.logEmitter$.emit(this.loggedUser);

        if (typeof (Storage) !== "undefined") {
            localStorage.removeItem("loggedUser");
            localStorage.removeItem("accessToken");
        }

        // if (typeof(Storage) !== "undefined")
        //     localStorage.clear();
    }

/*============================================================================*/
/*  USERS                                                                     */
/*============================================================================*/

    /**
     *
     */
    getMe() {
        let authorization: string = "Bearer " + this.accessToken;

        let headers: Headers = new Headers();
        headers.append("authorization", authorization);

        return this.http.get(this.bluzuApiBaseUrl + "/me", { headers: headers })
            .map(res => res.json());
        // .catch((error:any) => Observable.throw(error.json().error || 'Server error')); //...errors if
    }

    /**
     *
     */
    getUser(id: string): Promise<User> {
        return this.http.get(this.bluzuApiBaseUrl + "/users/" + id)
            .map(res => res.json()).toPromise();
    }

    /**
     *
     */
    getAllUsers(): Observable<User[]> {
        return this.http.get(this.bluzuApiBaseUrl + "/all/users")
            .map(res => res.json());
    }

    /**
     *
     */
    searchUsers(searchString: string): Observable<User[]> {
        return this.http.get(`${this.bluzuApiBaseUrl}/search/users/${searchString}`)
            .map(res => res.json());
    }

    getUserPlaylists(id: string): Observable<Playlist[]> {
        return this.http.get(`${this.bluzuApiBaseUrl}/users/${id}/playlists`)
            .map(res => res.json());
    }

    getUserTracks(id: string): Observable<Track[]> {
        return this.http.get(`${this.bluzuApiBaseUrl}/users/${id}/tracks`)
            .map(res => res.json());
    }

/*============================================================================*/
/*  TRACKS                                                                    */
/*============================================================================*/

    /**
     *
     */
    getMeTracks(selectQuery: string = undefined): Observable<Track[]> {
        let headers: Headers = new Headers();
        headers.append("authorization", "Bearer " + this.accessToken);

        let query: string = "";

        if (selectQuery) {
            query = "?select=" + selectQuery;
        }

        return this.http.get(`${this.bluzuApiBaseUrl}/me/tracks/${query}`, { headers: headers })
            .map(res => res.json().tracks);
    }

    /**
     *
     */
    getAllTracks() {
        return this.http.get(this.bluzuApiBaseUrl + "/all/tracks")
            .map(res => res.json());
    }

    /**
    *
    */
    newTrack(): Promise<Track> {
        let authorization: string = `Bearer ${this.accessToken}`;

        let headers: Headers = new Headers();
        headers.append("content-type", "application/json");
        headers.append("authorization", authorization);

        let body = {};

        return this.http.post(this.bluzuApiBaseUrl + "/me/track", body, { headers: headers })
            .map(res => {
                let track: Track = res.json().track;
                this.newTrack$.emit(track);
                return track;
            }).toPromise();
        // .catch((error:any) => Observable.throw(error.json().error || 'Server error')); //...errors if
    }

    /**
     *
     */
    updateTrack(trackId: string, trackMetaData: Object): Promise<Track> {
        let authorization: string = `Bearer ${this.accessToken}`;

        let headers: Headers = new Headers();
        headers.append("content-type", "application/json");
        headers.append("authorization", authorization);

        let body = trackMetaData;

        return this.http.put(`${this.bluzuApiBaseUrl}/me/track/${trackId}`, body, { headers: headers })
            .map(res => res.json().track).toPromise();
        // .catch((error:any) => Observable.throw(error.json().error || 'Server error')); //...errors if
    }

    /**
     *
     */
    deleteTrack(trackId: string): Promise<boolean> {
        let authorization: string = `Bearer ${this.accessToken}`;

        let headers: Headers = new Headers();
        headers.append("content-type", "application/json");
        headers.append("authorization", authorization);

        return this.http.delete(`${this.bluzuApiBaseUrl}/me/track/${trackId}`, { headers: headers })
            .map(res => {
                if (res.json().message === "Track deleted") {
                    this.deletedTrack$.emit(trackId);
                    Materialize.toast("Track deleted", 2000, "blue-bg rounded");
                    return true;
                }
                else {
                    Materialize.toast("An error occured while deleting track", 2000, "red-bg rounded");
                    return false;
                }
            }).toPromise();
        // .catch((error:any) => Observable.throw(error.json().error || 'Server error')); //...errors if
    }

    /**
     *
     */
    searchTracks(searchString: string): Observable<Track[]> {
        return this.http.get(`${this.bluzuApiBaseUrl}/search/tracks/${searchString}`)
            .map(res => res.json());
    }

/*============================================================================*/
/*  PLAYLIST                                                                  */
/*============================================================================*/

    /**
     *
     */
     newPlaylist(metadata: any): Promise<Playlist> {
         let headers: Headers = new Headers();
         headers.append("authorization", "Bearer " + this.accessToken);
         headers.append("content-type", "application/json")

         return this.http.post(`${this.bluzuApiBaseUrl}/me/playlist`, metadata, { headers: headers })
             .map(res => {
                 let playlist: Playlist = res.json().playlist;
                 this.newPlaylist$.emit(playlist);
                 return playlist;
             }).toPromise();
     }

     /**
      *
      */
     deletePlaylist(playlistId: string): Promise<boolean> {
         let headers: Headers = new Headers();
         headers.append("content-type", "application/json");
         headers.append("authorization", `Bearer ${this.accessToken}`);

         return this.http.delete(`${this.bluzuApiBaseUrl}/me/playlist/${playlistId}`, { headers: headers })
             .map(res => {
                 if (res.json().message === "Playlist deleted") {
                     this.deletedPlaylist$.emit(playlistId);
                     Materialize.toast("Playlist deleted", 2000, "blue-bg rounded");
                     return true;
                 }
                 else {
                     Materialize.toast("An error occured while deleting playlist", 2000, "red-bg rounded");
                     return false;
                 }
             }).toPromise();
         // .catch((error:any) => Observable.throw(error.json().error || 'Server error')); //...errors if
     }

    /**
     *
     */
    getMePlaylists(selectQuery: string = undefined): Observable<Playlist[]> {
        let headers: Headers = new Headers();
        headers.append("authorization", "Bearer " + this.accessToken);

        let query: string = "";

        if (selectQuery) {
            query = "?select=" + selectQuery;
        }

        return this.http.get(`${this.bluzuApiBaseUrl}/me/playlists/${query}`, { headers: headers })
            .map(res => res.json().playlists);
    }

    /**
     *
     */
    getAllPlaylists() {
        return this.http.get(this.bluzuApiBaseUrl + "/all/playlists")
            .map(res => res.json());
    }

    /**
     * TODO
     */
    getSomePlaylists(ids: string[]) {
        return this.http.get(this.bluzuApiBaseUrl + "/all/playlists")
            .map(res => res.json());
    }

    /**
     * TODO
     */
    getPlaylist(id: string): Promise<Playlist> {
        return this.http.get(this.bluzuApiBaseUrl + "/playlists/" + id)
            .map(res => res.json()).toPromise();
    }

    /**
     *
     */
    addTrackToPlaylist(trackId: string, playlistId: string): Promise<Playlist> {
        let headers: Headers = new Headers();
        headers.append("content-type", "application/json");
        headers.append("authorization", `Bearer ${this.accessToken}`);

        let body = {
            trackId: trackId,
            playlistId: playlistId
        };

        return this.http.put(`${this.bluzuApiBaseUrl}/me/playlist/${playlistId}/add-track`, body, { headers: headers })
            .map((res:any) => {
                Materialize.toast(res.json().message, 2500, "blue-bg rounded");
                return res.json().playlist;
            }).toPromise()
            .catch((err:any) => {
                Materialize.toast(err.json().message, 3000, "red-bg rounded");
                return {};
            });
    }

    /**
     * TODO
     */
    getPlaylistTracks(id: string): Observable<Track[]> {
        return this.http.get(this.bluzuApiBaseUrl + "/playlists/" + id + "/tracks")
            .map(res => res.json());
    }

    /**
     *
     */
    searchPlaylists(searchString: string): Observable<Playlist[]> {
        return this.http.get(`${this.bluzuApiBaseUrl}/search/playlists/${searchString}`)
            .map(res => res.json());
    }

/*============================================================================*/
/*  UPLOAD                                                                    */
/*============================================================================*/

    /**
     *
     */
    uploadTrackImage(trackId: string, files: any): Promise<Track> {
        let headers: Headers = new Headers();
        // headers.append("content-type", "multipart/form-data");
        headers.append("authorization", `Bearer ${this.accessToken}`);

        let formData = new FormData();
        if (files.length == 1) {
            formData.append("trackImage", files[0]);
        }
        else {
            return Promise.resolve({});
        }

        return this.http.post(`${this.bluzuApiBaseUrl}/me/track/${trackId}/uploadImage`, formData, { headers: headers })
            // return this.http.post(`${this.bluzuApiBaseUrl}/me/track/${trackId}/uploadImage`, formData, {headers: headers})
            .map(res => {
                // if(res.json().message === "Track deleted")
                //     return true;
                // else
                //     return false;
                return res.json().track;
            }).toPromise();
    }

    /**
     *
     */
    uploadTrackAudio(trackId: string, files: any): Promise<Track> {
        let headers: Headers = new Headers();
        // headers.append("content-type", "multipart/form-data");
        headers.append("authorization", `Bearer ${this.accessToken}`);

        let formData = new FormData();
        if (files.length == 1) {
            formData.append("trackAudio", files[0]);
        }
        else {
            return Promise.resolve({});
        }

        return this.http.post(`${this.bluzuApiBaseUrl}/me/track/${trackId}/uploadAudio`, formData, { headers: headers })
            .map(res => {
                return res.json().track;
            }).toPromise();
    }

    /**
     *
     */
    uploadPlaylistImage(playlistId: string, files: any): Promise<Playlist> {
        let headers: Headers = new Headers();
        headers.append("authorization", `Bearer ${this.accessToken}`);

        let formData = new FormData();
        if (files.length == 1) {
            formData.append("playlistImage", files[0]);
        }
        else {
            return Promise.resolve({});
        }

        return this.http.post(`${this.bluzuApiBaseUrl}/me/playlist/${playlistId}/uploadImage`, formData, { headers: headers })
            .map(res => {
                return res.json().playlist;
            }).toPromise();
    }

}
