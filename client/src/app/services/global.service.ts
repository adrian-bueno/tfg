import { Injectable, Output, EventEmitter  } from '@angular/core';

declare let window: any;

@Injectable()
export class GlobalService {

    @Output() contentWidth$: EventEmitter<number> = new EventEmitter();
    @Output() contextMenu$: EventEmitter<any> = new EventEmitter();

    broadcastNewContentWidth(width: number) {
        this.contentWidth$.emit(width);
    }

    contextMenu(data: any) {
        this.contextMenu$.emit(data);
    }

    runningOnMobile(): boolean {
        let userAgent = navigator.userAgent || navigator.vendor || window.opera;

        // Windows Phone must come first because its UA also contains "Android"
        if (/windows phone/i.test(userAgent)) {
            return true;
        }

        if (/android/i.test(userAgent)) {
            return true;
        }

        // iOS detection from: http://stackoverflow.com/a/9039885/177710
        if (/iPad|iPhone|iPod/.test(userAgent) && !window.MSStream) {
            return true;
        }

        return false;
    }

}
