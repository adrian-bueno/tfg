import { Injectable, Output, EventEmitter } from '@angular/core';
// import dashjs from "dashjs";
// import { MediaPlayer } from "dashjs";
// let dashjs: any = require("dashjs");

declare let dashjs: any;
declare var navigator: any;
declare var MediaMetadata: any;

import { Track, User, Playlist } from '../models';
import { BluzuService } from "./bluzu.service";

@Injectable()
export class PlayerService {

    audio: any = new Audio();  // Audio element that plays one track
    dashPlayer: any = dashjs.MediaPlayer().create();
    playQueue: Track[] = [];        // List of tracks
    playingTrackIndex: number = 0;  // Index of playing track in playQueue
    repeatState: string = "off";    // Repeat: off/all/one

    // Used to emit the track that is playing.
    @Output() newPlayingTrack$: EventEmitter<Track> = new EventEmitter();
    // Emits how many tracks have been added to play queue.
    @Output() newPlayQueue$: EventEmitter<Track[]> = new EventEmitter();
    // Emited when play queue has ended and repeatState = off
    @Output() ended$: EventEmitter<string> = new EventEmitter();

    /**
     *
     */
    constructor(private bluzuService: BluzuService) {

        // Initialize dashjs player, the second parameter is undefined because
        // I do not want to pass it a url for now (I attach the url in
        // this.loadNewTrack())
        // And disable debug messages (which are activated by default)
        this.dashPlayer.getDebug().setLogToBrowserConsole(false);
        this.dashPlayer.initialize(this.audio, undefined, false);

        // Load last playing track and last playQueue
        this.loadPlayQueueOfLocalStorage();

        // Catchs event triggered when window is closed.
        // It saves player service data in local storage.
        // These data is loaded again when the app starts.
        window.onbeforeunload = () => this.savePlayQueueOnLocalStorage();

        // Function called when audio ends playing completely.
        // If the played track was the last in queue and repeatState = "all"
        // it starts playing again from the beginning of the playQueue.
        // If the played track was not the last in playQueue it continue playing
        // the next track.
        // Otherwise it stops playing.
        this.audio.onended = () => this.onEndedAudio();

        //
        this.bluzuService.deletedTrack$.subscribe(deletedTrackId => this.onDeletedTrack(deletedTrackId));
    }

    /**
     * mediaSession and MediaMetadata are only available in Chrome version 57+
     */
    mediaNotification() {
        if ('mediaSession' in navigator) {

            let track: Track = this.playQueue[this.playingTrackIndex];
            let artwork: any[] = [];

            for (let q of track.image.qualities) {
                artwork.push({
                    src: `${track.image.url}/${q}.${track.image.format}`,
                    sizes: `${q}x${q}`,
                    type: `image/${track.image.format}`
                });
            }

            navigator.mediaSession.metadata = new MediaMetadata({
                title: track.name,
                artist: track.artistsN,
                // album: 'Album',
                artwork: artwork
            });

            navigator.mediaSession.setActionHandler('play', () => this.play());
            navigator.mediaSession.setActionHandler('pause', () => this.pause());
            // navigator.mediaSession.setActionHandler('seekbackward', function() { });
            // navigator.mediaSession.setActionHandler('seekforward', function() { });
            navigator.mediaSession.setActionHandler('previoustrack', () => this.playPrevious());
            navigator.mediaSession.setActionHandler('nexttrack', () => this.playNext());
        }
    }

    /**
     *
     */
    onEndedAudio(): void {
        if ((this.repeatState === "all" && this.playingTrackIndex === this.playQueue.length - 1) ||
            (this.repeatState !== "one" && this.playingTrackIndex !== this.playQueue.length - 1)) {
            this.playNext();
        }
        else if (this.repeatState === "off" && this.playingTrackIndex === this.playQueue.length - 1) {
            this.playingTrackIndex = 0;
            this.loadNewTrack(this.playQueue[this.playingTrackIndex]);
            this.ended$.emit("Play queue has ended and repeat is off.");
        }
    }

    /**
     *
     */
    onDeletedTrack(trackId: string): void {
        this.removeFromPlayQueue(trackId);
    }

    /**
     *
     */
    loadPlayQueueOfLocalStorage(): void {
        if (typeof (Storage) !== "undefined") {
            try {
                this.playQueue = JSON.parse(localStorage.getItem("playQueue"));
                this.playingTrackIndex = JSON.parse(localStorage.getItem("playingTrackIndex"));
                this.loadNewTrack(this.playQueue[this.playingTrackIndex]);
                this.repeatState = localStorage.getItem("repeatState");
                this.audio.currentTime = JSON.parse(localStorage.getItem("currentTime"));
                this.mediaNotification();

                if (this.repeatState === "one")
                    this.repeatOne();
            }
            catch (err) {
                // console.log("No saved data in local storage.");
                // console.log(err);

                this.playQueue = [];
                this.playingTrackIndex = 0;
                this.repeatState = "off";
            }
        }
        else {
            console.log("No Web Storage support..");
        }
    }

    /**
     *
     */
    savePlayQueueOnLocalStorage(): void {
        if (typeof (Storage) !== "undefined") {
            try {
                localStorage.setItem("playQueue", JSON.stringify(this.playQueue));
                localStorage.setItem("playingTrackIndex", this.playingTrackIndex.toString());
                localStorage.setItem("repeatState", this.repeatState);
                localStorage.setItem("currentTime", this.audio.currentTime.toString());
            }
            catch (err) {
                // console.log("Error saving data in local cache.");
                // console.log(err);
            }
        }
        else {
            console.log("No Web Storage support..");
        }
    }

    /**
     *
     */
    getPlayingTrack(): Track {
        return this.playQueue[this.playingTrackIndex];
    }

    /**
     *
     */
    getPlayingTrackAlbumId(): string {
        if (this.playQueue[this.playingTrackIndex] !== undefined)
            return this.playQueue[this.playingTrackIndex].albumId;
    }

    /**
     *
     */
    getPlayQueue(): Track[] {
        return this.playQueue;
    }

    /**
     * If track is in play queue it returns it index inside queue.
     * Else returns -1.
     */
    inPlayQueue(track: Track): number {

        if (!track) return -1;

        let index: number = 0;

        for (let queueTrack of this.playQueue) {
            if (queueTrack.id === track.id)
                return index;
            index++;
        }

        return -1;
    }

    /**
     *
     */
    addOneToPlayQueue(track: Track): void {
        this.playQueue[this.playQueue.length] = track;

        // TODO
        this.newPlayQueue$.emit(this.playQueue);
    }

    /**
     *
     */
    addOneToPlayQueueAndPlay(track: Track): void {
        this.addOneToPlayQueue(track);

        this.playingTrackIndex = 0;
        this.playNew(this.playQueue[this.playingTrackIndex]);
    }

    /**
     *
     */
    addToPlayQueue(listOfTracks: Track[]): void {
        for (let track of listOfTracks)
            this.playQueue[this.playQueue.length] = track;

        // TODO
        this.newPlayQueue$.emit(this.playQueue);
    }

    /**
     *
     */
    addToPlayQueueAndPlay(listOfTracks: Track[]): void {
        if (listOfTracks.length !== 0) {
            this.addToPlayQueue(listOfTracks);
            this.playingTrackIndex = 0;
            this.playNew(this.playQueue[this.playingTrackIndex]);
        }
    }

    /**
     *
     */
    removeFromPlayQueue(trackId: string): void {
        let i = 0;
        for (let track of this.playQueue) {
            if (track.id === trackId) {
                this.playQueue.splice(i, 1);
                this.newPlayQueue$.emit(this.playQueue);
                break;
            }
            i++;
        }
    }

    /**
     *
     */
    cleanPlayQueue(): void {
        this.playQueue.length = 0;
    }

    /**
     *
     */
    isEmptyPlayQueue(): boolean {
        if (this.playQueue.length === 0)
            return true;
        else
            return false;
    }

    /**
     *
     */
    loadNewTrack(track: Track): void {

        if (track) {
            if (track.url.dash) {
                // console.log(track);
                // TODO: this works but i think it can be improved (i dont know now how)
                // this.dashPlayer.initialize(this.audio, track.url.dash, false);
                this.dashPlayer.attachSource(track.url.dash);
            }
            else if (track.url.html5) {
                this.audio.src = track.url.html5;
                this.audio.load();
                this.audio.currentTime = 0;
            }
            else {
                this.playNext();
            }
        }
        else {
            this.audio.pause();
            this.audio.src = "";
            this.audio.currentTime = 0;
        }

        this.newPlayingTrack$.emit(track);
    }

    /**
     *
     */
    playNew(track: Track): void {
        this.loadNewTrack(track);
        this.mediaNotification();
        this.play();
    }

    /**
     * Checks if the track is in play queue.
     * If is in play queue then it starts playing.
     * If not, a new play queue is made with the playlist tracks and the track
     * starts playing.
     */
    playTrackPlaylist(track: Track, playlistTracks: Track[]): void {
        if (!track && !playlistTracks) return;

        let index: number = this.inPlayQueue(track);

        if (index > -1) { // track in queue
            this.playingTrackIndex = index;
            this.playNew(this.playQueue[this.playingTrackIndex]);
        }
        else { // track not in queue
            this.cleanPlayQueue();

            if (!track)
                index = 0;

            if (playlistTracks)
                this.addToPlayQueueAndPlay(playlistTracks);
            else
                this.addOneToPlayQueueAndPlay(track);
        }
    }

    /**
     * Plays the track in queue stored in the index passed by parameter.
     */
    playTrackIndex(queueTrackIndex: number): void {
        if (queueTrackIndex > 0 || queueTrackIndex < this.playQueue.length - 1) {
            this.playingTrackIndex = queueTrackIndex;
            this.playNew(this.playQueue[this.playingTrackIndex]);
        }
    }

    /**
     * If a track in playQueue has that id it starts playing.
     * Returns false if track is not in queue, true if it is.
     */
    playTrackId(trackId: string): boolean {

        let i = 0;

        for (let track of this.playQueue) {
            if (track.id === trackId) {
                this.playingTrackIndex = i;
                this.playNew(track);
                return true;
            }

            i++;
        }

        return false;
    }


    /**
     *
     */
    play(): void {
        this.audio.play();
    }

    /**
     *
     */
    pause(): void {
        this.audio.pause();
    }

    /**
     *
     */
    isPaused(): boolean {
        return this.audio.paused;
    }

    /**
     *
     */
    ended(): boolean {
        return this.audio.ended;
    }

    /**
     *
     */
    playNext(): void {
        this.playingTrackIndex++;
        if (this.playingTrackIndex === this.playQueue.length)
            this.playingTrackIndex = 0;

        this.playNew(this.playQueue[this.playingTrackIndex]);
    }

    /**
     *
     */
    playPrevious(): void {
        this.playingTrackIndex--;
        if (this.playingTrackIndex === -1)
            this.playingTrackIndex = this.playQueue.length - 1;

        this.playNew(this.playQueue[this.playingTrackIndex]);
    }

    /**
     *
     */
    getDuration(): number {
        // return this.audio.duration;
        if (this.playQueue[this.playingTrackIndex] === undefined)
            return 0;
        else
            return this.playQueue[this.playingTrackIndex].duration;
    }

    /**
     *
     */
    setCurrentTime(timeInSeconds: number): void {
        this.audio.currentTime = timeInSeconds;
    }

    /**
     *
     */
    getCurrentTime(): number {
        return this.audio.currentTime;
    }

    /**
     *
     */
    repeatOff(): void {
        this.audio.loop = false;
        this.repeatState = "off";
    }

    /**
     *
     */
    repeatOne(): void {
        this.audio.loop = true;
        this.repeatState = "one";
    }

    /**
     *
     */
    repeatAll(): void {
        this.audio.loop = false;
        this.repeatState = "all";
    }

    /**
     *
     */
    getRepeatState(): string {
        return this.repeatState;
    }

    /**
     * TODO
     */
    suffleOn(): void { }

    /**
     * TODO
     */
    suffleOff(): void { }

    /**
     * TODO
     */
    volumeUp(): void { }

    /**
     * TODO
     */
    volumeDown(): void { }

}
