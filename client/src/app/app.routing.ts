import { NgModule }  from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import {
    HomeComponent,
    PlaylistComponent,
    SearchComponent,
    UploadComponent,
    UserComponent,
    UserHomeComponent,
    UserMusicComponent,
    UserPlaylistsComponent,
    UserSimilarArtistsComponent,
    UserAboutComponent
} from './components';

const appRoutes: Routes = [
    {
        path: 'search',
        component: SearchComponent
    },
    {
        path: 'playlist/:id',
        component: PlaylistComponent
    },
    {
        path: 'upload',
        component: UploadComponent
    },
    {
        path: 'user/:id',
        component: UserComponent,
        children: [
            {
                path: '',
                redirectTo: 'home',
                pathMatch: 'full'
            },
            {
                path: 'home',
                component: UserHomeComponent
            },
            {
                path: 'music',
                component: UserMusicComponent
            },
            {
                path: 'playlists',
                component: UserPlaylistsComponent
            },
            {
                path: 'similar-artists',
                component: UserSimilarArtistsComponent
            },
            {
                path: 'about',
                component: UserAboutComponent
            },
        ]
    },
    // {
    //     path: ':id',
    //     redirectTo: 'user/:id',
    //     pathMatch: 'full'
    // },
    {
        path: '',
        component: HomeComponent
    },
    {
        path: '**',
        redirectTo: '/',
    },
];

@NgModule({
    imports: [
        RouterModule.forRoot(appRoutes)
    ],
    exports: [
        RouterModule
    ]
})

export class AppRoutingModule {}
